package mx.com.sinergiadigital.taxsichoferandroid.Background.Request;

import java.io.Serializable;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseRequestInterface;

public class LocationDriverRequest implements Serializable,WSBaseRequestInterface {
    public String id_conductor;
    public String latitud;
    public String longitud;

    public LocationDriverRequest(String id_conductor, String latitud, String longitud) {
        this.id_conductor = id_conductor;
        this.latitud = latitud;
        this.longitud = longitud;
    }
}
