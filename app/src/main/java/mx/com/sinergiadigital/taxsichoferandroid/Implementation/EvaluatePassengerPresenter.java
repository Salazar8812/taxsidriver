package mx.com.sinergiadigital.taxsichoferandroid.Implementation;

import android.support.v7.app.AppCompatActivity;

import mx.com.sinergiadigital.taxsichoferandroid.Background.BaseWSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.EvaluatePassengerRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.TakeTripRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.GetDataTripResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.WSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSCallback;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.MessageUtils;

public class EvaluatePassengerPresenter extends TaxiPresenter implements WSCallback {

    private EvaluatePasseengerCallback mEvaluatePasseengerCallback;

    public interface EvaluatePasseengerCallback{
        void OnSuccessEvaluate();
    }

    public EvaluatePassengerPresenter(AppCompatActivity appCompatActivity, EvaluatePasseengerCallback mEvaluatePasseengerCallback) {
        super(appCompatActivity);
        this.mEvaluatePasseengerCallback = mEvaluatePasseengerCallback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void evaluate(String id_conductor,String id_viaje, String valoracion,String comentario){
        EvaluatePassengerRequest takeTripRequest = new EvaluatePassengerRequest(id_conductor,id_viaje,valoracion,comentario);
        mWSManager.requestWs(GetDataTripResponse.class, WSManager.WS.EVUALATE_PASSENGER, takeTripRequest);
    }

    private void OnSuccessEvaluate(GetDataTripResponse mGetDataTripResponse){
        if(mGetDataTripResponse != null) {
            if (mGetDataTripResponse.resultado.get(0).resultado == 1) {
                MessageUtils.stopProgress();
                mEvaluatePasseengerCallback.OnSuccessEvaluate();
            } else {
                MessageUtils.stopProgress();
                MessageUtils.toast(mAppCompatActivity, "Ocurrio un error al enviar la evaluacion, intente nuevamente");
            }
        }else{
            MessageUtils.stopProgress();
            MessageUtils.toast(mAppCompatActivity, "Ocurrio un error al enviar la evaluacion, intente nuevamente o mas tarde");
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if(requestUrl.equals(WSManager.WS.EVUALATE_PASSENGER)){
            OnSuccessEvaluate((GetDataTripResponse) baseResponse);
        }
    }
}
