package mx.com.sinergiadigital.taxsichoferandroid.Utils;

public class NotificationEvent  {
    public String[] notification;

    public NotificationEvent(String[] notification) {
        this.notification = notification;
    }

    public String[] getNotification() {
        return notification;
    }
}
