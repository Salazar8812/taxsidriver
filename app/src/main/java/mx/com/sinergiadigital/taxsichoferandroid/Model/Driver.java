package mx.com.sinergiadigital.taxsichoferandroid.Model;

import com.google.gson.annotations.SerializedName;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;

import java.io.Serializable;

public class Driver implements Serializable, WSBaseResponseInterface {

    @SerializedName("id")
    public String id;

    @SerializedName("nombre")
    public String mNombre;

    @SerializedName("apellidos")
    public String nApellidos;

    @SerializedName("email")
    public String mCorreo;

    @SerializedName("telefono")
    public String mTelefono;

    @SerializedName("direccion")
    public String mDireccion;

    @SerializedName("unidad")
    public String mUnidad;

    @SerializedName("url_foto")
    public String mUrl;

    @SerializedName("modelo")
    public String mModelo;

    @SerializedName("placas")
    public String mPlacas;

}
