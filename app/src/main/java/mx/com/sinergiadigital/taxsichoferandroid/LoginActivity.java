package mx.com.sinergiadigital.taxsichoferandroid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import com.bumptech.glide.Glide;

import mx.com.sinergiadigital.taxsichoferandroid.Implementation.BasePresenter;
import mx.com.sinergiadigital.taxsichoferandroid.Implementation.LoginPresenter;
import mx.com.sinergiadigital.taxsichoferandroid.InternalData.PrefsTaxsi;
import mx.com.sinergiadigital.taxsichoferandroid.Service.GPSClass;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.BaseActivity;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.EditTextValidator;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.FormValidator;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.MessageUtils;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.Regex;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by charlssalazar on 23/05/18.
 */

public class LoginActivity extends BaseActivity{
    @BindView(R.id.mEmailEditText)
    public EditText mEmailEditText;
    @BindView(R.id.mPasswordEditText)
    public EditText mPasswordEditText;
    @BindView(R.id.mBackgroundImageView)
    public ImageView mBackgroundImageView;
    private LocationManager locationManager;
    private PrefsTaxsi mPrefsTaxsi;
    private LoginPresenter mLoginPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);

        mPrefsTaxsi = new PrefsTaxsi(this);

        setFieldValidator();

        Glide.with(this).load(R.drawable.ic_login_bakcground).into(mBackgroundImageView);

        requestPermission();
    }

    public void setFieldValidator(){
        /*mEmailEditText.setText("ernesto@hotmail.com");
        mPasswordEditText.setText("1234");*/

        mFormValidator = new FormValidator(this);
        mFormValidator.addValidators(
                new EditTextValidator(mEmailEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mPasswordEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );
    }


    @Override
    protected BasePresenter getPresenter() {
        mLoginPresenter = new LoginPresenter(this);
        return mLoginPresenter;
    }

    private void initServiceGPS(){
        Intent i =new Intent(getApplicationContext(),GPSClass.class);
        startService(i);
    }

    @OnClick(R.id.mLoginButton)
    public void OnClickLogin(){
        if(mFormValidator.isValid()) {
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            mLoginPresenter.doLogin(mEmailEditText.getText().toString().trim(), mPasswordEditText.getText().toString().trim());
        }
    }

    private void requestPermission() {
        if (ActivityCompat.checkSelfPermission(LoginActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(LoginActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1); //Any number can be used    }
        }else{
            gpsCheckStatus();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED
                && grantResults[2] == PackageManager.PERMISSION_GRANTED
                && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                    gpsCheckStatus();
                }else{
                    MessageUtils.toast(this, "Es necesario aceptar los permisos");
                    this.finish();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        starLocation();
    }

    public void gpsCheckStatus(){
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(intent,0);
        }else{
            starLocation();
        }
    }

    public void starLocation(){
        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String latitude = intent.getStringExtra(GPSClass.EXTRA_LATITUDE);
                        String longitude = intent.getStringExtra(GPSClass.EXTRA_LONGITUDE);
                        Log.e("Latitude", latitude);
                        Log.e("Longitud", longitude);
                        mPrefsTaxsi.saveData("latitude",latitude);
                        mPrefsTaxsi.saveData("longitude",longitude);
                    }
                }, new IntentFilter(GPSClass.ACTION_LOCATION_BROADCAST)
        );
        initServiceGPS();
    }

}
