package mx.com.sinergiadigital.taxsichoferandroid.Background.Response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;
import mx.com.sinergiadigital.taxsichoferandroid.Model.Resultado;

public class CloseSessionResponse implements WSBaseResponseInterface {
    @SerializedName("resultado")
    public List<Resultado> mListResultado = new ArrayList<>();
}
