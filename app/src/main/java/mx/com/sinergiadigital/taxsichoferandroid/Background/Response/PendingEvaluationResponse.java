package mx.com.sinergiadigital.taxsichoferandroid.Background.Response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;

public class PendingEvaluationResponse implements Serializable, WSBaseResponseInterface {
    @SerializedName("viaje_pendiente_calif")
    public List<Travel_pending> mTravel_pending= new ArrayList<>();

    @SerializedName("resultado")
    public List<Resultado> mResultado = new ArrayList<>();

    public class Travel_pending implements Serializable, WSBaseResponseInterface{
        @SerializedName("id")
        public String id;
    }

    public class Resultado implements Serializable, WSBaseResponseInterface{
        @SerializedName("resultado")
        public String resultado;
    }
}
