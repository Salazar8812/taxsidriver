package mx.com.sinergiadigital.taxsichoferandroid.Background.Request;

import com.google.gson.annotations.SerializedName;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseRequestInterface;

import java.io.Serializable;

public class TakeTripRequest implements Serializable,WSBaseRequestInterface {
    @SerializedName("id_conductor")
    public String id_conductor;

    @SerializedName("id_viaje")
    public String id_viaje;

    public TakeTripRequest(String id_conductor, String id_viaje) {
        this.id_conductor = id_conductor;
        this.id_viaje = id_viaje;
    }
}
