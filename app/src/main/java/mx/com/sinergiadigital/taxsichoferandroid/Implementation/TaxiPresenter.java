package mx.com.sinergiadigital.taxsichoferandroid.Implementation;

import android.support.v7.app.AppCompatActivity;

import mx.com.sinergiadigital.taxsichoferandroid.Background.BaseWSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSCallback;
import mx.com.sinergiadigital.taxsichoferandroid.R;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.MessageUtils;

public abstract class TaxiPresenter extends BasePresenter implements WSCallback {
    protected BaseWSManager mWSManager;


    @Override
    public void onResume() {
        super.onResume();
    }

    public TaxiPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mWSManager = initWSManager();
    }

    public abstract BaseWSManager initWSManager();

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mWSManager != null) mWSManager.onDestroy();
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        MessageUtils.stopProgress();
        if (messageError.trim().equals("")) {
            MessageUtils.toast(mContext, R.string.dialog_intern_error);
        } else {
            MessageUtils.toast(mContext, messageError);
        }
    }

    @Override
    public void onErrorConnection() {
        MessageUtils.stopProgress();
        MessageUtils.toast(mContext, R.string.dialog_error_connection);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}
