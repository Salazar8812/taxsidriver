package mx.com.sinergiadigital.taxsichoferandroid.Background.Request;

import java.io.Serializable;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseRequestInterface;

public class CalcularTarifaRequest implements Serializable, WSBaseRequestInterface{
    public String distancia;
    public String tiempo;

    public CalcularTarifaRequest(String distancia, String tiempo) {
        this.distancia = distancia;
        this.tiempo = tiempo;
    }
}
