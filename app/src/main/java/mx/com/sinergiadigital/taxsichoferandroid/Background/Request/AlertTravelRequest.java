package mx.com.sinergiadigital.taxsichoferandroid.Background.Request;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseRequestInterface;

public class AlertTravelRequest implements WSBaseRequestInterface {
    public String latitud;
    public String longitud;

    public AlertTravelRequest(String latitud, String longitud) {
        this.latitud = latitud;
        this.longitud = longitud;
    }
}
