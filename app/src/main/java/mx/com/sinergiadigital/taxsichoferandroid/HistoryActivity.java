package mx.com.sinergiadigital.taxsichoferandroid;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.OnClick;
import mx.com.sinergiadigital.taxsichoferandroid.Adapters.HistoryAdapter;
import mx.com.sinergiadigital.taxsichoferandroid.Implementation.BasePresenter;
import mx.com.sinergiadigital.taxsichoferandroid.Implementation.HistoryPresenter;
import mx.com.sinergiadigital.taxsichoferandroid.InternalData.PrefsTaxsi;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import mx.com.sinergiadigital.taxsichoferandroid.Model.Viaje;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.BaseActivity;

/**
 * Created by charlssalazar on 28/05/18.
 */

public class HistoryActivity extends BaseActivity implements HistoryPresenter.HistoryCallback {
    @BindView(R.id.mHistoryRecyclerView)
    public RecyclerView mHistoryRecyclerView;
    @BindView(R.id.mLeftMenuLinearLayout)
    public LinearLayout mLeftMenuLinearLayout;
    @BindView(R.id.mTitleToolBarTextField)
    public TextView mTitleToolBarTextField;
    @BindView(R.id.mBackImageView)
    public ImageView mBackImageView;
    @BindView(R.id.mRichIconImageView)
    public ImageView mRichIconImageView;
    @BindView(R.id.mRightMenuLinearLayout)
    public LinearLayout mRightMenuLinearLayout;

    private HistoryAdapter mHistoryAdapter;
    private HistoryPresenter mHistoryPresenter;
    public static Activity historyActivity;
    private PrefsTaxsi mPrefsTaxsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_activity);
        ButterKnife.bind(this);
        historyActivity = this;
        mPrefsTaxsi = new PrefsTaxsi(this);

        mHistoryRecyclerView = (RecyclerView) findViewById(R.id.mHistoryRecyclerView);
        mHistoryRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        setToolBar();
        loadHistory();
    }

    public void loadHistory(){
        mHistoryPresenter.getHistorial(mPrefsTaxsi.getData("id_conductor"),"3");
    }

    @Override
    public BasePresenter getPresenter() {
        mHistoryPresenter = new HistoryPresenter(this,this);
        return mHistoryPresenter;
    }

    public void setToolBar(){
        setTitle("Historial");
        mBackImageView.setImageResource(R.drawable.ic_back_icon);
        mRichIconImageView.setVisibility(View.INVISIBLE);
    }

    public void setTitle(String mTitle){
        mTitleToolBarTextField.setText(mTitle);
    }

    @OnClick(R.id.mBackImageView)
    public void OnClickBack(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void OnSuccessHistory(List<Viaje> listViaje) {
        mHistoryAdapter = new HistoryAdapter(this, listViaje);
        mHistoryRecyclerView.setAdapter(mHistoryAdapter);
    }
}
