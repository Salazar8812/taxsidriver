package mx.com.sinergiadigital.taxsichoferandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.firebase.messaging.FirebaseMessaging;;
import mx.com.sinergiadigital.taxsichoferandroid.Implementation.BasePresenter;
import mx.com.sinergiadigital.taxsichoferandroid.Implementation.CloseSessionPresenter;
import mx.com.sinergiadigital.taxsichoferandroid.InternalData.PrefsTaxsi;
import mx.com.sinergiadigital.taxsichoferandroid.Model.Driver;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.BaseActivity;

/**
 * Created by charlssalazar on 28/05/18.
 */

public class ProfileActivity extends BaseActivity implements CloseSessionPresenter.CloseSessionCallback {
    private List<Driver> mListDriver = new ArrayList<>();
    @BindView(R.id.mNameDriverTextView)
    public TextView mNameDriverTextView;
    @BindView(R.id.mPhoneTextView)
    public TextView mPhoneTextView;
    @BindView(R.id.mEmailDriverTextView)
    public TextView mEmailDriverTextView;
    @BindView(R.id.mStatusScoreTxtView)
    public TextView mStatusScoreTxtView;
    @BindView(R.id.mExperienceTextView)
    public TextView mExperienceTextView;
    @BindView(R.id.mProfileImageView)
    public ImageView mProfileImageView;
    @BindView(R.id.mLeftMenuLinearLayout)
    public LinearLayout mLeftMenuLinearLayout;
    @BindView(R.id.mTitleToolBarTextField)
    public TextView mTitleToolBarTextField;
    @BindView(R.id.mBackImageView)
    public ImageView mBackImageView;
    @BindView(R.id.mRichIconImageView)
    public ImageView mRichIconImageView;
    @BindView(R.id.mRightMenuLinearLayout)
    public LinearLayout mRightMenuLinearLayout;

    public static Activity profileActivity;
    private PrefsTaxsi prefsTaxsi;

    private CloseSessionPresenter mCloseSessionPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        prefsTaxsi = new PrefsTaxsi(this);
        ButterKnife.bind(this);
        profileActivity = this;
        retrieveData();
        setToolBar();
    }

    @Override
    protected BasePresenter getPresenter() {
        mCloseSessionPresenter = new CloseSessionPresenter(this,this);
        return mCloseSessionPresenter;
    }

    public void setToolBar(){
        setTitle("Perfil");
        mBackImageView.setImageResource(R.drawable.ic_back_icon);
        mRichIconImageView.setVisibility(View.INVISIBLE);
    }

    public void setTitle(String mTitle){
        mTitleToolBarTextField.setText(mTitle);
    }

    public void retrieveData(){
        mListDriver = (List<Driver>) getIntent().getExtras().getSerializable("infoDriver");
        mNameDriverTextView.setText(mListDriver.get(0).mNombre + " " +mListDriver.get(0).nApellidos );
        mPhoneTextView.setText(mListDriver.get(0).mTelefono);
        mEmailDriverTextView.setText(mListDriver.get(0).mCorreo);
        if(!mListDriver.get(0).mUrl.isEmpty()) {
            Glide.with(this).load(mListDriver.get(0).mUrl).into(mProfileImageView);
        }
    }

    @OnClick(R.id.mCloseSession)
    public void OnClickCloseSession(){
        unSubscribe();
        mCloseSessionPresenter.CloseSession(prefsTaxsi.getData("id_conductor"));
    }

    @OnClick(R.id.mChangePasswordTexView)
    public void OnClickChangePassword(){
        startActivity(new Intent(this,ChangePasswordActivity.class));
    }

    @OnClick(R.id.mBackImageView)
    public void OnClickBack(){
        onBackPressed();
    }

    @OnClick(R.id.mHistoryTripTextView)
    public void OnClickHistory(){
        startActivity(new Intent(this, HistoryActivity.class));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void subscribe(){
        Log.d("Subscripcion Firebase", "Habilitado");
        FirebaseMessaging.getInstance().subscribeToTopic("c"+prefsTaxsi.getData("id_conductor"));
    }

    public void unSubscribe(){
        Log.e("Subscripcion Firebase","Deshabilitado temporalmente");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("c"+prefsTaxsi.getData("id_conductor"));
    }

    @Override
    public void OnCloseSession() {
        removeData();
        Intent intent = new Intent(this,LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void removeData(){
        prefsTaxsi.removeData("email");
    }
}
