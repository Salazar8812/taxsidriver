package mx.com.sinergiadigital.taxsichoferandroid.Implementation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;

import mx.com.sinergiadigital.taxsichoferandroid.Background.BaseWSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.LoginRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.LoginResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.WSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSCallback;
import mx.com.sinergiadigital.taxsichoferandroid.InternalData.PrefsTaxsi;
import mx.com.sinergiadigital.taxsichoferandroid.MainContainerActivity;
import mx.com.sinergiadigital.taxsichoferandroid.Model.Viaje;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.MessageUtils;

import java.io.Serializable;

public class LoginPresenter extends TaxiPresenter implements WSCallback{
    private PrefsTaxsi mPrefsTaxsi;
    private String email;
    private String pass;

    public LoginPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void doLogin(String mEmail, String mPassword){
        email = mEmail;
        pass = mPassword;
        LoginRequest loginRequest = new LoginRequest(mEmail,mPassword);
        mWSManager.requestWs(LoginResponse.class, WSManager.WS.LOGIN, loginRequest);
    }

    @Override
    public void onSuccessLoadResponse(String requestURL, WSBaseResponseInterface baseResponse) {
        onSuccessLogin(requestURL,(LoginResponse) baseResponse);
    }

    public void onSuccessLogin(String requestURL, LoginResponse mLoginResponse){
        mPrefsTaxsi = new PrefsTaxsi(mAppCompatActivity);
        MessageUtils.stopProgress();
        if(requestURL.equals(WSManager.WS.LOGIN)){
            if(mLoginResponse.mDriver.get(0).id.equals("0")){
                MessageUtils.toast(mAppCompatActivity,"Conductor no encontrado");
            }else{
                mPrefsTaxsi.saveData("id_conductor",mLoginResponse.mDriver.get(0).id);
                mPrefsTaxsi.saveData("email",email);
                mPrefsTaxsi.saveData("pass",pass);
                mPrefsTaxsi.saveData("nombre",mLoginResponse.mDriver.get(0).mNombre);
                mPrefsTaxsi.saveData("apellidos",mLoginResponse.mDriver.get(0).nApellidos);

                saveDataLoginResponse(mLoginResponse);

                Intent intent = new Intent(mAppCompatActivity,MainContainerActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("driverInfo", (Serializable) mLoginResponse.mDriver);

                mAppCompatActivity.startActivity(intent);
            }
        }
    }



    public void saveDataLoginResponse(LoginResponse trip){
        Gson gson = new Gson();
        String json = gson.toJson(trip);
        mPrefsTaxsi.saveData("loginResponse",json);
    }


}
