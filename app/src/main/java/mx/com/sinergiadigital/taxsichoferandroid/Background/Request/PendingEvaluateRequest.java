package mx.com.sinergiadigital.taxsichoferandroid.Background.Request;

import java.io.Serializable;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseRequestInterface;

public class PendingEvaluateRequest implements Serializable, WSBaseRequestInterface {
    public String id_conductor;

    public PendingEvaluateRequest(String id_conductor) {
        this.id_conductor = id_conductor;
    }
}
