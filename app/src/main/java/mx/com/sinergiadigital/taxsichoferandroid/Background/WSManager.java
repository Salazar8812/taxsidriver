package mx.com.sinergiadigital.taxsichoferandroid.Background;


import mx.com.sinergiadigital.taxsichoferandroid.Background.Definitions.TaxsiAppDefinitions;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.AlertTravelRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.CalcularTarifaRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.ChangePasswordRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.CloseSessionRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.EndTripRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.EvaluatePassengerRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.LocationDriverRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.LoginRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.MessageNotificationRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.PendingEvaluateRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.StatusDriverRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.TakeTripRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseRequestInterface;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by charlssalazar on 29/05/18.
 */

public class WSManager extends BaseWSManager {

    public static boolean DEBUG_ENABLED = false;

    public static WSManager init() {
        return new WSManager();
    }

    public class WS {
        public static final String LOGIN = "login";
        public static final String TAKETRIP = "trip";
        public static final String INIT_TRIP = "initTrip";
        public static final String END_TRIP = "endTrip";
        public static final String EVUALATE_PASSENGER = "evaluate_passenger";
        public static final String GET_DATA_CONSULTING = "get_data_driver";
        public static final String GET_DATA_TRIP = "get_data_trip";
        public static final String CANCEL_TRIP = "cancel_trio";
        public static final String SEND_LOCATION_DRIVER = "send_location_driver";
        public static final String HISTORY_TRAVELS = "history";
        public static final String GET_ESTATUS_DRIVER = "get_status_driver";
        public static final String GET_PENDING_TRAVEL = "get_pending_travel";
        public static final String GET_DATA_TRAVEL_EN_RUTA_DE_RECOLECCION = "get_data_en_ruta_de_recoleccion";
        public static final String GET_DATA_TRABEL_EN_VIAJE_DESTINO = "get_data_en_viaje_destino";
        public static final String GET_DATA_CANCEL_LISTENER = "get_data_travel_cancel_listener";
        public static final String GET_DATA_FROM_NOTIFICATION = "get_data_from_notificacion";
        public static final String SEND_NOTIFICATION = "send_notification";
        public static final String CHANGE_PASSWORD  = "change_password";
        public static final String CALCULAR_TARIFA  = "calcular_tarifa";
        public static final String GET_CENTRAL_PHONES = "get_central_phone";
        public static final String ALERT_NEAR_TRAVEL = "alert_near_travel";
        public static final String CLOSE_SESSION = "close_session";

    }

    @Override
    protected Call<ResponseBody> getWebService(String webServiceValue, WSBaseRequestInterface wsBaseRequest) {
        Call<ResponseBody> call = null;
        TaxsiAppDefinitions taxiWebServicesDefinition = WebServices.taxsiAppDefinitions();
        switch (webServiceValue) {
            case WS.LOGIN:
                LoginRequest loginRequest = (LoginRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.login(loginRequest.email,loginRequest.password);
                break;
            case WS.TAKETRIP:
                TakeTripRequest takeTripRequest = (TakeTripRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.takeTrip(takeTripRequest.id_conductor,takeTripRequest.id_viaje);
                break;

            case WS.INIT_TRIP:
                TakeTripRequest initTrip = (TakeTripRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.initTrip(initTrip.id_conductor,initTrip.id_viaje);
                break;

            case WS.END_TRIP:
                EndTripRequest endTripRequest = (EndTripRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.endTrip(endTripRequest.id_conductor, endTripRequest.id_viaje,endTripRequest.latitud_llegada_real, endTripRequest.longitud_llegada_real);
                break;

            case WS.EVUALATE_PASSENGER:
                EvaluatePassengerRequest evaluatePassengerRequest = (EvaluatePassengerRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.evaluatePassenger(evaluatePassengerRequest.id_conductor, evaluatePassengerRequest.id_viaje,evaluatePassengerRequest.valoracion,evaluatePassengerRequest.comentario);
                break;

            case WS.HISTORY_TRAVELS:
                TakeTripRequest historyRequest =  (TakeTripRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.getHistory(historyRequest.id_conductor, historyRequest.id_viaje);
                break;

            case WS.GET_DATA_TRIP:
                TakeTripRequest getDataTrip = (TakeTripRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.getDataTrip(getDataTrip.id_conductor,getDataTrip.id_viaje);
                break;
            case WS.CANCEL_TRIP:
                TakeTripRequest cancelRequest = (TakeTripRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.cancelTrip(cancelRequest.id_conductor,cancelRequest.id_viaje);
                break;

            case WS.SEND_LOCATION_DRIVER:
                LocationDriverRequest locationDriverRequest = (LocationDriverRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.sendLocationDriver(locationDriverRequest.id_conductor,locationDriverRequest.latitud, locationDriverRequest.longitud);
                break;
            case WS.GET_ESTATUS_DRIVER:
                StatusDriverRequest statusDriverRequest = (StatusDriverRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.getStatusDriver(statusDriverRequest.id_driver);
                break;

            case WS.GET_PENDING_TRAVEL:
                PendingEvaluateRequest pendingEvaluateRequest = (PendingEvaluateRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.getCalificacionPendiente(pendingEvaluateRequest.id_conductor);
                break;

            case WS.GET_DATA_CONSULTING:
                TakeTripRequest getDataTravel = (TakeTripRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.getDataTrip(getDataTravel.id_conductor,getDataTravel.id_viaje);
                break;

            case WS.GET_DATA_TRAVEL_EN_RUTA_DE_RECOLECCION:
                TakeTripRequest mRequestInCollection = (TakeTripRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.getDataTrip(mRequestInCollection.id_conductor,mRequestInCollection.id_viaje);
                break;

            case WS.GET_DATA_TRABEL_EN_VIAJE_DESTINO:
                TakeTripRequest mRequestInCourse = (TakeTripRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.getDataTrip(mRequestInCourse.id_conductor,mRequestInCourse.id_viaje);
                break;

            case WS.GET_DATA_CANCEL_LISTENER:
                TakeTripRequest mRequestCancelLister = (TakeTripRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.getDataTrip(mRequestCancelLister.id_conductor,mRequestCancelLister.id_viaje);

                break;

            case WS.GET_DATA_FROM_NOTIFICATION:
                TakeTripRequest mRequest = (TakeTripRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.getDataTrip(mRequest.id_conductor,mRequest.id_viaje);
                break;

            case WS.SEND_NOTIFICATION:
                MessageNotificationRequest mRequestNotification = (MessageNotificationRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.sendNotification(mRequestNotification.id_conductor,mRequestNotification.msj);
                break;

            case WS.CHANGE_PASSWORD:
                ChangePasswordRequest mChangePasswordRequest = (ChangePasswordRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.changePassword(mChangePasswordRequest.id_conductor,mChangePasswordRequest.viejoPass, mChangePasswordRequest.nuevoPass);
                break;

            case WS.CALCULAR_TARIFA:
                CalcularTarifaRequest mCalcularTarifaRequest = (CalcularTarifaRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.calcularTarifa(mCalcularTarifaRequest.distancia,mCalcularTarifaRequest.tiempo);
                break;

            case WS.GET_CENTRAL_PHONES:
                call = taxiWebServicesDefinition.getTelefonosCentral();
                break;

            case WS.ALERT_NEAR_TRAVEL:
                AlertTravelRequest alertTravelRequest = (AlertTravelRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.getAlertNearTravel(alertTravelRequest.latitud,alertTravelRequest.longitud);
                break;

            case WS.CLOSE_SESSION:
                CloseSessionRequest closeSessionRequest = (CloseSessionRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.closeSession(closeSessionRequest.id_usuario);
                break;
        }
        return call;
    }

    @Override
    protected Call<ResponseBody> getQueryWebService(String webServiceValue, String requestValue) {
        return null;
    }

    @Override
    protected String getJsonDebug(String requestUrl) {
        return null;
    }

    @Override
    protected boolean getErrorDebugEnabled() {
        return false;
    }

    @Override
    protected boolean getDebugEnabled() {
        return false;
    }
}
