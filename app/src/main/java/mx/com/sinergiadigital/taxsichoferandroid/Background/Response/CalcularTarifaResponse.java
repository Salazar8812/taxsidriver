package mx.com.sinergiadigital.taxsichoferandroid.Background.Response;

import android.support.annotation.StringRes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;

public class CalcularTarifaResponse implements Serializable, WSBaseResponseInterface {
    @SerializedName("tarifa")
    public List<Tarifa> tarifa = new ArrayList<>();


    public class Tarifa{
        @SerializedName("tarifa")
        public String tarifa;
    }
}
