package mx.com.sinergiadigital.taxsichoferandroid.Utils;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import mx.com.sinergiadigital.taxsichoferandroid.Implementation.BasePresenter;

public abstract class BaseActivity extends AppCompatActivity {
    public Toolbar mToolbar;
    private BasePresenter[] mPresenters;
    private BasePresenter mPresenter;
    public FormValidator mFormValidator;

    public BaseActivity() {
    }



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.mPresenters = this.getPresenters();
        this.mPresenter = this.getPresenter();
        if (this.mPresenters != null) {
            BasePresenter[] var2 = this.mPresenters;
            int var3 = var2.length;

            for(int var4 = 0; var4 < var3; ++var4) {
                BasePresenter basePresenter = var2[var4];
                if (basePresenter != null) {
                    basePresenter.onCreate();
                }
            }
        }

        if (this.mPresenter != null) {
            this.mPresenter.onCreate();
        }

    }

    protected BasePresenter getPresenter() {
        return null;
    }

    protected BasePresenter[] getPresenters() {
        return null;
    }

    public void setSubtitle(String subtitle) {
        if (this.getSupportActionBar() != null) {
            this.getSupportActionBar().setSubtitle(subtitle);
        }

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            this.onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        /*if (this.toolbarEnabled) {
            View toolbar = this.findToolbar(this.findViewById(16908290));
            if (toolbar == null) {
                throw new InflateException("You must add a Toolbar on the Activity or setToolbarEnabled(false) before setContentView()");
            }

            this.mToolbar = (Toolbar)toolbar;
            this.setupToolbar();
        }*/

    }

    protected void onStart() {
        super.onStart();
        if (this.mPresenters != null) {
            BasePresenter[] var1 = this.mPresenters;
            int var2 = var1.length;

            for(int var3 = 0; var3 < var2; ++var3) {
                BasePresenter basePresenter = var1[var3];
                if (basePresenter != null) {
                    basePresenter.onStart();
                }
            }
        }

        if (this.mPresenter != null) {
            this.mPresenter.onStart();
        }

    }

    protected void onResume() {
        super.onResume();
        if (this.mPresenters != null) {
            BasePresenter[] var1 = this.mPresenters;
            int var2 = var1.length;

            for(int var3 = 0; var3 < var2; ++var3) {
                BasePresenter basePresenter = var1[var3];
                if (basePresenter != null) {
                    basePresenter.onResume();
                }
            }
        }

        if (this.mPresenter != null) {
            this.mPresenter.onResume();
        }

    }

    protected void onPause() {
        super.onPause();
        if (this.mPresenters != null) {
            BasePresenter[] var1 = this.mPresenters;
            int var2 = var1.length;

            for(int var3 = 0; var3 < var2; ++var3) {
                BasePresenter basePresenter = var1[var3];
                if (basePresenter != null) {
                    basePresenter.onPause();
                }
            }
        }

        if (this.mPresenter != null) {
            this.mPresenter.onPause();
        }

    }

    protected void onStop() {
        super.onStop();
        if (this.mPresenters != null) {
            BasePresenter[] var1 = this.mPresenters;
            int var2 = var1.length;

            for(int var3 = 0; var3 < var2; ++var3) {
                BasePresenter basePresenter = var1[var3];
                if (basePresenter != null) {
                    basePresenter.onStop();
                }
            }
        }

        if (this.mPresenter != null) {
            this.mPresenter.onStop();
        }

    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.mPresenters != null) {
            BasePresenter[] var1 = this.mPresenters;
            int var2 = var1.length;

            for(int var3 = 0; var3 < var2; ++var3) {
                BasePresenter basePresenter = var1[var3];
                if (basePresenter != null) {
                    basePresenter.onDestroy();
                }
            }
        }

        if (this.mPresenter != null) {
            this.mPresenter.onDestroy();
        }

    }
}
