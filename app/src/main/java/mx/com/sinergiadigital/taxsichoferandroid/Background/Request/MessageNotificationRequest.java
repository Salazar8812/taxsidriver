package mx.com.sinergiadigital.taxsichoferandroid.Background.Request;

import java.io.Serializable;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseRequestInterface;

public class MessageNotificationRequest implements Serializable,WSBaseRequestInterface {
    public String id_conductor;
    public String msj;

    public MessageNotificationRequest(String id_conductor, String msj) {
        this.id_conductor = id_conductor;
        this.msj = msj;
    }
}
