package mx.com.sinergiadigital.taxsichoferandroid.Background.Request;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseRequestInterface;

public class CloseSessionRequest implements WSBaseRequestInterface {
    public String id_usuario;

    public CloseSessionRequest(String id_usuario) {
        this.id_usuario = id_usuario;
    }
}
