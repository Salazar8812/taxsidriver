package mx.com.sinergiadigital.taxsichoferandroid;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mx.com.sinergiadigital.taxsichoferandroid.Implementation.BasePresenter;
import mx.com.sinergiadigital.taxsichoferandroid.Implementation.SendNotificationPresenter;
import mx.com.sinergiadigital.taxsichoferandroid.InternalData.PrefsTaxsi;
import mx.com.sinergiadigital.taxsichoferandroid.Model.Viaje;
import mx.com.sinergiadigital.taxsichoferandroid.R;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.BaseActivity;

public class CallProfileActivity extends BaseActivity implements SendNotificationPresenter.CallCentralCallback {
    @BindView(R.id.mLeftMenuLinearLayout)
    public LinearLayout mLeftMenuLinearLayout;
    @BindView(R.id.mTitleToolBarTextField)
    public TextView mTitleToolBarTextField;
    @BindView(R.id.mBackImageView)
    public ImageView mBackImageView;
    @BindView(R.id.mRightMenuLinearLayout)
    public LinearLayout mRightMenuLinearLayout;
    @BindView(R.id.mRichIconImageView)
    public ImageView mRichIconImageView;
    @BindView(R.id.mPhoneUserClientTextView)
    public TextView mPhoneUserClientTextView;
    @BindView(R.id.mNameClienteTextView)
    public TextView mNameClienteTextView;
    @BindView(R.id.mInCourseButton)
    public Button mInCourseButton;
    @BindView(R.id.mNearButton)
    public Button mNearButton;
    @BindView(R.id.mArriveButton)
    public Button mArriveButton;

    String phone;
    private SendNotificationPresenter mSendNotificationPresenter;
    private PrefsTaxsi mPrefsTaxsi;
    private String mIdTrip;
    private Viaje mViaje;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.call_profile_activity);
        ButterKnife.bind(this);
        mNameClienteTextView.setText(getIntent().getStringExtra("nameClient"));
        mPhoneUserClientTextView.setText(getIntent().getStringExtra("phoneClient"));
        mIdTrip = getIntent().getStringExtra("id_trip");
        setTitle("Perfil de Cliente");
        mPrefsTaxsi = new PrefsTaxsi(this);
        setViews();
        mSendNotificationPresenter.getCentralPhone();
        getIdPasajero();
    }

    private void getIdPasajero(){
        mViaje = getDataTrip();
        hideButton();
    }

    public Viaje getDataTrip(){
        Gson gson = new Gson();
        String json = mPrefsTaxsi.getData("viaje");
        Viaje obj = gson.fromJson(json, Viaje.class);
        return obj;
    }

    @Override
    protected BasePresenter getPresenter() {
        mSendNotificationPresenter = new SendNotificationPresenter(this,this);
        return mSendNotificationPresenter;
    }

    @OnClick(R.id.mBackImageView)
    public void OnClickBack(){
        onBackPressed();
    }

    public void hideButton(){
        if(mViaje.idPasajero.equals("0")){
            mInCourseButton.setVisibility(View.INVISIBLE);
            mNearButton.setVisibility(View.INVISIBLE);
            mArriveButton.setVisibility(View.INVISIBLE);
        }else{
            mInCourseButton.setVisibility(View.VISIBLE);
            mNearButton.setVisibility(View.VISIBLE);
            mArriveButton.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.mContentCallPhone)
    public void OnClickPhoneCall(){
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        startActivity(intent);
    }

    @OnClick(R.id.mInCourseButton)
    public void OnClickCourse(){
        mSendNotificationPresenter.sendNotification(mPrefsTaxsi.getData("id_conductor"),"1");
    }

    @OnClick(R.id.mNearButton)
    public void OnClickNear(){
        mSendNotificationPresenter.sendNotification(mPrefsTaxsi.getData("id_conductor"),"2");
    }

    @OnClick(R.id.mArriveButton)
    public void OnClickArriveButton(){
        mSendNotificationPresenter.sendNotification(mPrefsTaxsi.getData("id_conductor"),"3");
    }


    private void setViews(){
        mBackImageView.setImageResource(R.drawable.ic_icon_back);
        mRightMenuLinearLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void OnCallCentral(String phone) {
        this.phone = phone;
    }
}
