package mx.com.sinergiadigital.taxsichoferandroid.Implementation;

import android.support.v7.app.AppCompatActivity;

import mx.com.sinergiadigital.taxsichoferandroid.Background.BaseWSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.ChangePasswordRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.PendingEvaluateRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.ChangePasswordResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.PendingEvaluationResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.WSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSCallback;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.MessageUtils;

public class ChangePasswordPresenter extends TaxiPresenter implements WSCallback {



    public ChangePasswordPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
    }

    public void changePassword(String id_conductor, String viejoPass, String nuevoPass){
        ChangePasswordRequest mChangePasswordRequest = new ChangePasswordRequest(id_conductor,viejoPass,nuevoPass);
        mWSManager.requestWs(ChangePasswordResponse.class, WSManager.WS.CHANGE_PASSWORD,mChangePasswordRequest);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if(requestUrl.equals(WSManager.WS.CHANGE_PASSWORD)){
            OnSuccessChangePassword((ChangePasswordResponse) baseResponse);
        }
    }

    public void OnSuccessChangePassword(ChangePasswordResponse mChangePasswordResponse){
        MessageUtils.stopProgress();
        try {
            switch (mChangePasswordResponse.resultado.get(0).resultado) {
                case "0":
                    MessageUtils.toast(mAppCompatActivity, "Error al cambiar contraseña, intente mas tarde");
                    break;
                case "1":
                    MessageUtils.toast(mAppCompatActivity, "La contraseña se ha cmabiado exitósamente");
                    break;
                case "2":
                    MessageUtils.toast(mAppCompatActivity, "Error al cambiar password, olvido su contraseña?");
                    break;
                default:
                    break;
            }
        }catch (NullPointerException e){
            e.printStackTrace();
            MessageUtils.toast(mAppCompatActivity, "Ocurrio un error al tratar de cambiar el password, intente mas tarde");
        }
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }
}
