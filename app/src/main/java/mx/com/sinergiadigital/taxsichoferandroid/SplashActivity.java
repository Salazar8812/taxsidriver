package mx.com.sinergiadigital.taxsichoferandroid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.LoginResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Implementation.BasePresenter;
import mx.com.sinergiadigital.taxsichoferandroid.Implementation.LoginPresenter;
import mx.com.sinergiadigital.taxsichoferandroid.InternalData.PrefsTaxsi;
import mx.com.sinergiadigital.taxsichoferandroid.Service.GPSClass;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.BaseActivity;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.BusManager;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.LocationRetrieve;

public class SplashActivity extends BaseActivity {
    @BindView(R.id.mSplashImageView)
    ImageView mSplashImageView;
    private LocationManager locationManager;
    private PrefsTaxsi mPrefsTaxsi;
    private LoginPresenter mLoginPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_activity);
        ButterKnife.bind(this);
        mPrefsTaxsi = new PrefsTaxsi(this);

        Glide.with(this).load(R.drawable.ic_splash).into(mSplashImageView);
        TimerTask splashTask = new TimerTask() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        };

        gpsCheckStatus();

        if(isLogged()){
            TimerTask splashTask2 = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            LaunchMain();
                            //mLoginPresenter.doLogin(mPrefsTaxsi.getData("email").trim(), mPrefsTaxsi.getData("pass").trim());
                        }
                    });
                }
            };
            new Timer().schedule(splashTask2, 5000);
        }else{
            new Timer().schedule(splashTask, 3000);
        }

    }

    public void LaunchMain(){
        Intent intent = new Intent(this,MainContainerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        try {
            if (getLoginResponse() != null) {
                if (getLoginResponse().mDriver != null) {
                    intent.putExtra("driverInfo", (Serializable) getLoginResponse().mDriver);
                    startActivity(intent);
                } else {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            }else{
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        }catch (NullPointerException e){
            Intent i = new Intent(SplashActivity.this, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }

    }

    @Override
    protected BasePresenter getPresenter() {
        mLoginPresenter = new LoginPresenter(this);
        return mLoginPresenter;
    }

    public boolean isLogged(){
        if(mPrefsTaxsi.getData("email").isEmpty()){
            return false;
        }else{
            return true;
        }
    }

    public LoginResponse getLoginResponse(){

        Gson gson = new Gson();
        String json = mPrefsTaxsi.getData("loginResponse");
        LoginResponse obj = gson.fromJson(json, LoginResponse.class);
        return obj;
    }



    public void gpsCheckStatus(){
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(intent,0);
        }else{
            starLocation();
        }
    }


    public void starLocation(){
        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String latitude = intent.getStringExtra(GPSClass.EXTRA_LATITUDE);
                        String longitude = intent.getStringExtra(GPSClass.EXTRA_LONGITUDE);
                        Log.e("Latitude", latitude);
                        Log.e("Longitud", longitude);
                        mPrefsTaxsi.saveData("latitude",latitude);
                        mPrefsTaxsi.saveData("longitude",longitude);
                    }
                }, new IntentFilter(GPSClass.ACTION_LOCATION_BROADCAST)
        );

        initServiceGPS();
    }

    private void initServiceGPS(){
        Intent i =new Intent(getApplicationContext(),GPSClass.class);
        startService(i);
    }

}
