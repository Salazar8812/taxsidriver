package mx.com.sinergiadigital.taxsichoferandroid.Fragments;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RemoteViews;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Random;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.BaseFragment;
import mx.com.sinergiadigital.taxsichoferandroid.Implementation.LoginPresenter;
import mx.com.sinergiadigital.taxsichoferandroid.InternalData.PrefsTaxsi;
import mx.com.sinergiadigital.taxsichoferandroid.R;
import mx.com.sinergiadigital.taxsichoferandroid.Service.GPSClass;
import mx.com.sinergiadigital.taxsichoferandroid.SplashActivity;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.DrawMarker;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.DrawRouteMaps;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.LocationUtils;

/**
 * Created by charlssalazar on 25/05/18.
 */

public class MapFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener,View.OnClickListener {
    public GoogleMap map;
    private PrefsTaxsi mPrefsTaxsi;
    private ImageView mLocationImaeView;
    public Marker mk = null;
    private int markerCount;
    private LocationManager locationManager;
    private LoginPresenter mLoginPresenter;
    private boolean firstMoveDraw = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.map_fragment, container, false);
        mPrefsTaxsi = new PrefsTaxsi(getActivity());
        markerCount=0;
        mLocationImaeView = (ImageView) v.findViewById(R.id.mLocationImaeView);
        mLocationImaeView.setOnClickListener(this);
        SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);

        return v;
    }

    private static int getRequestCode() {
        Random rnd = new Random();
        return 100 + rnd.nextInt(900000);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        Location drawPoint = new Location("");
        try {
            drawPoint.setLatitude(Double.parseDouble(mPrefsTaxsi.getData("latitude")));
            drawPoint.setLongitude(Double.parseDouble(mPrefsTaxsi.getData("longitude")));
            createMarker(drawPoint);
        }catch (NumberFormatException e){
            e.printStackTrace();
            gpsCheckStatus();
            Location drawPoint2 = new Location("");
            drawPoint2.setLatitude(0.0);
            drawPoint2.setLongitude(0.0);
            createMarker(drawPoint2);
        }

    }

    public void drawRoute(LatLng mFrom, LatLng mArrive, Location mOrigin){
        try {
            LatLng origin = mFrom;
            LatLng destination = new LatLng(mArrive.latitude, mArrive.longitude);

            if(firstMoveDraw) {
                map.clear();
                DrawRouteMaps.getInstance(getActivity())
                        .draw(origin, destination, map);

                Location prevLoc = mOrigin;
                Location newLoc = prevLoc ;
                // float bearing = prevLoc.bearingTo(newLoc) ;

                DrawMarker.getInstance(getActivity()).drawOrigin(map,mFrom,destination,R.mipmap.ic_car_icon_t, "Mi Ubicación"/*,bearing*/);
                DrawMarker.getInstance(getActivity()).drawDestination(map, destination, R.mipmap.ic_marker_map, "Destino");

                LatLngBounds bounds = new LatLngBounds.Builder()
                        .include(origin)
                        .include(destination).build();
                Point displaySize = new Point();
                getActivity().getWindowManager().getDefaultDisplay().getSize(displaySize);

                firstMoveDraw = false;
                map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 250, 30));
            }else{
                DrawMarker.getInstance(getActivity()).drawOrigin(map,mFrom,destination,R.mipmap.ic_car_icon_t, "Mi Ubicación"/*,bearing*/);
            }
        } catch (NullPointerException e){
            e.printStackTrace();
        } catch (RuntimeException e){
            e.printStackTrace();
        }
    }

    public void cleanMap(){
        map.clear();
    }

    public void createMarker(Location driverLocation){
        try {
            map.clear();
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        try {
            int height = 120;
            int width = 120;
            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.mipmap.ic_car_icon_t);
            Bitmap b = bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

            //float bearing = (float) bearingBetweenLocations(new LatLng(driverLocation.getLatitude(),driverLocation.getLongitude()),new LatLng(driverLocation.getLatitude(),driverLocation.getLongitude()));

            mk = map.addMarker(new MarkerOptions()
                    .position(new LatLng(driverLocation.getLatitude(),driverLocation.getLongitude()))
                    .title("")
                    .flat(true)
            //.rotation(bearing)
                    .icon(BitmapDescriptorFactory.fromBitmap((smallMarker))));

            //animateMarker(driverLocation,mk);
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(driverLocation.getLatitude(),driverLocation.getLongitude()), 16.5f));

        }catch (NullPointerException | NumberFormatException e){
            e.printStackTrace();
        }
    }

    public static void animateMarker(final Location destination, final Marker marker) {
        if (marker != null) {
            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            final float startRotation = marker.getRotation();

            final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(1000); // duration 1 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        marker.setRotation(computeRotation(v, startRotation, destination.getBearing()));
                    } catch (Exception ex) {
                        // I don't care atm..
                    }
                }
            });

            valueAnimator.start();
        }
    }

    private static float computeRotation(float fraction, float start, float end) {
        float normalizeEnd = end - start; // rotate start to 0
        float normalizedEndAbs = (normalizeEnd + 360) % 360;

        float direction = (normalizedEndAbs > 180) ? -1 : 1; // -1 = anticlockwise, 1 = clockwise
        float rotation;
        if (direction > 0) {
            rotation = normalizedEndAbs;
        } else {
            rotation = normalizedEndAbs - 360;
        }

        float result = fraction * rotation + start;
        return (result + 360) % 360;
    }
    private interface LatLngInterpolator {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolator {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }


    @Override
    public void onClick(View view) {
        if(R.id.mLocationImaeView == view.getId()){
            try {
                int height = 120;
                int width = 120;
                BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.mipmap.ic_car_icon_t);
                Bitmap b = bitmapdraw.getBitmap();
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

                LatLng driverLocation = new LatLng(Double.parseDouble(mPrefsTaxsi.getData("latitude")), Double.parseDouble(mPrefsTaxsi.getData("longitude")));
                Marker marcador = map.addMarker(new MarkerOptions()
                        .position(driverLocation)
                        .title("")
                        .icon(BitmapDescriptorFactory.fromBitmap((smallMarker))));

                map.animateCamera(CameraUpdateFactory.newLatLngZoom(driverLocation, 16.5f));
            }catch (NullPointerException | NumberFormatException e){
                e.printStackTrace();
            }
        }
    }


    private double bearingBetweenLocations(LatLng latLng1,LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }

    private void initServiceGPS(){
        Intent i =new Intent(getActivity().getApplicationContext(),GPSClass.class);
        getActivity().startService(i);
    }

    public void gpsCheckStatus(){
        locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(intent,0);
        }else{
            starLocation();
        }
    }

    public void starLocation(){
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String latitude = intent.getStringExtra(GPSClass.EXTRA_LATITUDE);
                        String longitude = intent.getStringExtra(GPSClass.EXTRA_LONGITUDE);
                        Log.e("Latitude", latitude);
                        Log.e("Longitud", longitude);
                        mPrefsTaxsi.saveData("latitude",latitude);
                        mPrefsTaxsi.saveData("longitude",longitude);
                    }
                }, new IntentFilter(GPSClass.ACTION_LOCATION_BROADCAST)
        );

        initServiceGPS();
    }

    public void setfirstMoveDraw(){
        firstMoveDraw = true;
    }
}
