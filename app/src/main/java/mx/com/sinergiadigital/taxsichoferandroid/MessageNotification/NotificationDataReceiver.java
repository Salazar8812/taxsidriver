package mx.com.sinergiadigital.taxsichoferandroid.MessageNotification;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import java.util.Random;

import mx.com.sinergiadigital.taxsichoferandroid.InternalData.PrefsTaxsi;
import mx.com.sinergiadigital.taxsichoferandroid.R;
import mx.com.sinergiadigital.taxsichoferandroid.SplashActivity;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.BusManager;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.Helper;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.NotificationEvent;

public class NotificationDataReceiver extends WakefulBroadcastReceiver {

    private final String TAG = "FirebaseDataReceiver";
    private static PrefsTaxsi mPrefsTaxsi;
    private static String id_viaje;
    private String title;
    private String mensaje;
    private Context context;

    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "I'm in!!!");
        mPrefsTaxsi = new PrefsTaxsi(context);

        this.context = context;


            if (intent.getExtras() != null) {
                for (String key : intent.getExtras().keySet()) {
                    Object value = intent.getExtras().get(key);
                    Log.e("FirebaseDataReceiver", "Key: " + key + " Value: " + value);
                    if (key.equals("id_viaje")) {
                        id_viaje = String.valueOf(value);
                    }

                    if (key.equals("title")) {
                        title = String.valueOf(value);
                    }

                    if (key.equals("body")) {
                        mensaje = String.valueOf(value);
                    }
                }
            }

        mPrefsTaxsi.saveData("id_viaje",id_viaje);

        if (Helper.isAppRunning(context, "mx.com.sinergiadigital.taxsichoferandroid")) {
            BusManager.post(new NotificationEvent(new String[] {id_viaje,mensaje}));
        } else {
            mPrefsTaxsi.saveData("Notification","1");
            showNotification();
        }
    }


    public void callNotification(String title, String message){
        Intent myIntent = new Intent(context, SplashActivity.class);
        @SuppressLint("WrongConstant") PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, myIntent, Intent.FLAG_ACTIVITY_NEW_TASK);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Notification.Builder mNotifyBuilder = new Notification.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(false)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(getRequestCode(), mNotifyBuilder.build());
    }

    private static int getRequestCode() {
        Random rnd = new Random();
        return 100 + rnd.nextInt(900000);
    }

    public void showNotification(){
        NotificationManager mNotificationManager;
        NotificationCompat.Builder mBuilder;
        final String NOTIFICATION_CHANNEL_ID = "10001";

        /**Creates an explicit intent for an Activity in your app**/
        Intent resultIntent = new Intent(context , SplashActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(context,
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle(title)
                .setContentText("Nueva Alerta de viaje")
                .setAutoCancel(false)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(resultPendingIntent);

        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(0 /* Request Code */, mBuilder.build());

    }

}
