package mx.com.sinergiadigital.taxsichoferandroid.InternalData;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import mx.com.sinergiadigital.taxsichoferandroid.Model.Viaje;
import mx.com.sinergiadigital.taxsichoferandroid.R;

/**
 * Created by charlssalazar on 25/05/18.
 */

public class PrefsTaxsi {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public PrefsTaxsi(Context context){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
    }

    public void saveData(String key,String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public void removeData(String key){
        editor.remove(key).commit();
    }

    public String getData(String key){
        return sharedPreferences.getString(key, "");
    }
}
