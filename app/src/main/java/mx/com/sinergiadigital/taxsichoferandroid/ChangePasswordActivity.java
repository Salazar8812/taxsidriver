package mx.com.sinergiadigital.taxsichoferandroid;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mx.com.sinergiadigital.taxsichoferandroid.Implementation.BasePresenter;
import mx.com.sinergiadigital.taxsichoferandroid.Implementation.ChangePasswordPresenter;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.BaseActivity;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.EditTextValidator;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.FormValidator;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.Regex;

public class ChangePasswordActivity extends BaseActivity {
    @BindView(R.id.mPassOldEditText)
    public EditText mPassOldEditText;

    @BindView(R.id.mPassNewEditText)
    public EditText mPassNewEditText;

    @BindView(R.id.mPassRepeatEditText)
    public EditText mPassRepeatEditText;

    @BindView(R.id.mBackImageView)
    public ImageView mBackImageView;

    @BindView(R.id.mTitleToolBarTextField)
    public TextView mTitleToolBarTextField;

    @BindView(R.id.mRichIconImageView)
    public ImageView mRichIconImageView;

    private ChangePasswordPresenter mChangePasswordPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password_activity);
        ButterKnife.bind(this);
        setToolBar();
        mFormValidator = new FormValidator(this);
        mFormValidator.addValidators(
                new EditTextValidator(mPassOldEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mPassNewEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mPassRepeatEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
                );
    }

    @Override
    protected BasePresenter getPresenter() {
        mChangePasswordPresenter = new ChangePasswordPresenter(this);
        return mChangePasswordPresenter;
    }

    @OnClick(R.id.mSendPassButton)
    public void OnClickSendPass(){
        if(mFormValidator.isValid()){
            mChangePasswordPresenter.changePassword("",mPassOldEditText.getText().toString(), mPassNewEditText.getText().toString());
        }
    }

    @OnClick(R.id.mBackImageView)
    public void OnClickBack(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void setToolBar(){
        setTitle("Cambiar contraseña");
        mBackImageView.setImageResource(R.drawable.ic_back_icon);
        mRichIconImageView.setVisibility(View.INVISIBLE);
    }

    public void setTitle(String mTitle){
        mTitleToolBarTextField.setText(mTitle);
    }

}
