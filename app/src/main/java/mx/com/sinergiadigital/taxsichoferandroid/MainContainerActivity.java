package mx.com.sinergiadigital.taxsichoferandroid;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.location.Location;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.RequestResult;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Info;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.CalcularTarifaResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.GetDataTripResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.PendingEvaluationResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.StatusResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Fragments.MapFragment;
import mx.com.sinergiadigital.taxsichoferandroid.Implementation.BasePresenter;
import mx.com.sinergiadigital.taxsichoferandroid.Implementation.TakeTripPresenter;
import mx.com.sinergiadigital.taxsichoferandroid.InternalData.PrefsTaxsi;
import mx.com.sinergiadigital.taxsichoferandroid.Model.Driver;
import mx.com.sinergiadigital.taxsichoferandroid.Model.Viaje;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.BaseActivity;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.BusManager;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.ExpandAndCollapse;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.LocationRetrieve;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.MessageUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.NotificationEvent;

/**
 * Created by charlssalazar on 23/05/18.<
 */

public class MainContainerActivity extends BaseActivity implements MessageUtils.Callback, TakeTripPresenter.TakeTripCallBack{

    @BindView(R.id.mContainerFragment)
    public FrameLayout mContainerFragment;
    @BindView(R.id.mLeftMenuLinearLayout)
    public LinearLayout mLeftMenuLinearLayout;
    @BindView(R.id.mTitleToolBarTextField)
    public TextView mTitleToolBarTextField;
    @BindView(R.id.mBackImageView)
    public ImageView mBackImageView;
    @BindView(R.id.mContentNotificationLinerLayout)
    public LinearLayout mContentNotificationLinerLayout;
    @BindView(R.id.mPlaceOriginTextView)
    public TextView mPlaceOriginTextView;
    @BindView(R.id.mClientNameTextView)
    public TextView mClientNameTextView;
    @BindView(R.id.mRichIconImageView)
    public ImageView mRichIconImageView;
    @BindView(R.id.mRightMenuLinearLayout)
    public LinearLayout mRightMenuLinearLayout;
    @BindView(R.id.mContentDetailtravel)
    public LinearLayout mContentDetailtravel;
    @BindView(R.id.mContentTravelInfo)
    public LinearLayout mContentTravelInfo;
    @BindView(R.id.mContentInRoute)
    public LinearLayout mContentInRoute;
    @BindView(R.id.mStartTripButton)
    public Button mStartTripButton;
    @BindView(R.id.mCancelButtonTravel)
    public ImageButton mCancelButtonTravel;
    @BindView(R.id.mCallProfileLinearLayout)
    public LinearLayout mCallProfileLinearLayout;
    @BindView(R.id.mAlertNearClientButton)
    public Button mAlertNearClientButton;
    @BindView(R.id.mFromAddressTextView)
    public TextView mFromAddressTextView;
    @BindView(R.id.mToAddressTextView)
    public TextView mToAddressTextView;
    @BindView(R.id.mNameClientTextView)
    public TextView mNameClientTextView;
    @BindView(R.id.mInRouteTitleTextView)
    public TextView mInRouteTitleTextView;
    @BindView(R.id.mContentTraveDetailAndStartrip)
    public LinearLayout mContentTraveDetailAndStartrip;
    @BindView(R.id.mContentEndTrip)
    public LinearLayout mContentEndTrip;
    @BindView(R.id.mCostoTarifaTextView)
    public TextView mCostoTarifaTextView;

    @BindView(R.id.mContentButtonEndTravel)
    public LinearLayout mmContentButtonEndTravel;


    private FragmentManager FM;
    private List<Driver> mListDriver = new ArrayList<>();
    private boolean onService = false;
    private PrefsTaxsi mPrefsTaxsi;
    private String[] mNotificationEvent;
    private MapFragment mFragment;
    private TakeTripPresenter mTakeTripPresenter;
    private String mIdTrip;

    private String nameClient = "";
    private String phoneClient= "";
    private Viaje mViajeInCourse;

    private boolean isStartTrip = false;
    private boolean takeTripInRoute = false;
    private boolean takeTripInRouteFromHistory = false;
    private boolean expandOnlyTime = true;

    private String mLatitudPartida = "";
    private String mLongitudPartida = "";

    private String mLatitudLlegada = "";
    private String getmLongitudLlegada = "";

    private Timer timer;
    private TimerTask timerTask;

    private boolean mCallTravelFromNotification = false;
    private Location evenlocation;

    private static String KEY_LATITUD_LLEGADA = "latitudLlegada";
    private static String KEY_LONGITUD_LLEGADA = "longitudLlegada";

    private static String KEY_LATITUD_PARTIDA = "latitudPartida";
    private static String KEY_LONGITUD_PARTIDA = "latitudPartida";

    private static String KEY_PRICE_TARIFA = "tarifa";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu_activity);
        ButterKnife.bind(this);
        retrieveinfoDriver();

        mPrefsTaxsi = new PrefsTaxsi(this);

        mFragment = new MapFragment();

        loadFragment(mFragment);

        subscribe();

        calificacionPendiente();

        checkOnService();

        retrieveTarifa();

        getPoolTravelNotifications();

    }

    public void getPoolTravelNotifications(){
        mIdTrip = mPrefsTaxsi.getData("id_viaje");
        Log.e("ID_TRIP",mIdTrip);
        if(mIdTrip != "" || !mIdTrip.isEmpty()) {
            mTakeTripPresenter.getDataFromNotification(mPrefsTaxsi.getData("id_viaje"), mIdTrip);
        }
    }

    public void retrieveTarifa(){
        if(getTarifa() != null) {
            if(getDataTrip() != null) {
                try {
                    if (getDataTrip().idPasajero.equals("0")) {
                        mCostoTarifaTextView.setText("El costo aproximado de la tarifa es: $0.0 pesos");
                    } else {
                        mCostoTarifaTextView.setText("El costo aproximado de la tarifa es: $" + getTarifa() + " pesos");
                    }
                }catch (NullPointerException e){
                    mCostoTarifaTextView.setText("");
                }
            }else{
                mCostoTarifaTextView.setText("");
            }
        }
    }


    @Override
    protected BasePresenter getPresenter() {
        mTakeTripPresenter = new TakeTripPresenter(this,this);
        return mTakeTripPresenter;
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB_MR2)
    public void loadFragment(Fragment fragment) {
        mContainerFragment.removeAllViews();
        FM = getSupportFragmentManager();
        FragmentTransaction FT = FM.beginTransaction();
        FT.replace(R.id.mContainerFragment, fragment);
        FT.commit();
    }

    @OnClick(R.id.mLeftMenuLinearLayout)
    public void OnClickProfile(){
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra("infoDriver",(Serializable) mListDriver);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IsTripInCourse();
        verifyNotificationTrip();
        getIDTripFronInternalStorage();
        retrieveTarifa();
        try {
            BusManager.register(this);
        }catch (RuntimeException e){
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void OnGetNotification(final NotificationEvent event) {
        mCallTravelFromNotification = true;

        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
        toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);

        if(HistoryActivity.historyActivity != null){
            HistoryActivity.historyActivity.finish();
            ProfileActivity.profileActivity.finish();
        }else{
            if(ProfileActivity.profileActivity != null){
                ProfileActivity.profileActivity.finish();
            }
        }
        mNotificationEvent = event.getNotification();
        Log.d("Notification",mNotificationEvent[0]);
        mIdTrip = mNotificationEvent[0];
        Log.e("ID_TRIP",mIdTrip);
        mTakeTripPresenter.getDataFromNotification(mPrefsTaxsi.getData("id_conductor"), mIdTrip);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void OnLocationRetrieve(final LocationRetrieve eventLocation) {
        String latitude = String.valueOf(eventLocation.location.getLatitude());
        String longitude = String.valueOf(eventLocation.location.getLongitude());
        Log.e("Map Latitude", latitude);
        Log.e("Map Longitud", longitude);
        this.evenlocation = eventLocation.getLocation();
        mPrefsTaxsi.saveData("latitude",latitude);
        mPrefsTaxsi.saveData("longitude",longitude);
        if(takeTripInRoute){
            Log.e("Recolección","Recoger Al cliente");
            isArriveTrip(latitude,longitude);
            mTakeTripPresenter.sendLocationDriver(mPrefsTaxsi.getData("id_conductor"), latitude,longitude);
            mFragment.drawRoute(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)),new LatLng(Double.parseDouble(mLatitudPartida), Double.parseDouble(mLongitudPartida)),eventLocation.getLocation());
        }else if(isStartTrip) {
            Log.e("Viaje Inciado","Dejar a cliente");
            mTakeTripPresenter.sendLocationDriver(mPrefsTaxsi.getData("id_conductor"), latitude,longitude);
            mFragment.drawRoute(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)),new LatLng(Double.parseDouble(mLatitudLlegada), Double.parseDouble(getmLongitudLlegada)),eventLocation.getLocation());
        }else {
            if (!isStartTrip || !takeTripInRoute) {
                if (onService) {
                    mTakeTripPresenter.sendLocationDriver(mPrefsTaxsi.getData("id_conductor"), latitude, longitude);
                    mFragment.createMarker(eventLocation.location);
                }else{
                    mFragment.createMarker(eventLocation.location);
                }
            }
        }
    }

    @OnClick(R.id.mRichIconImageView)
    public void OnClickRight(){
        checkOnService();
    }

    @OnClick(R.id.mCancelButton)
    public void OnClickCancel(){
        ExpandAndCollapse.collapse(mContentNotificationLinerLayout);
    }

    @OnClick(R.id.mAccepButtonTrip)
    public void OnClickAcceptTrip(){
        mTakeTripPresenter.takeTrip(mPrefsTaxsi.getData("id_conductor"),mIdTrip);
    }

    @OnClick(R.id.mCallProfileLinearLayout)
    public void OnClickCallProfile(){
        Intent intent = new Intent(this, CallProfileActivity.class);
        intent.putExtra("nameClient",nameClient);
        intent.putExtra("phoneClient",phoneClient);
        intent.putExtra("id_trip", mIdTrip);
        startActivity(intent);
    }

    @OnClick(R.id.mCancelButtonTravel)
    public void OnClickCancelButton(){
        MessageUtils.LaunDialog(MainContainerActivity.this,"Taxsi","¿Está seguro que desea cancelar el viaje?",this);
    }

    @OnClick(R.id.mStartTripButton)
    public void OnClickStartTrip(){
        mFragment.setfirstMoveDraw();
        mTakeTripPresenter.initTrip(mPrefsTaxsi.getData("id_conductor"),mIdTrip);
    }

    @OnClick(R.id.mEndTripButton)
    public void OnClickEndTrip(){
        mFragment.setfirstMoveDraw();
        mTakeTripPresenter.endTrip(mPrefsTaxsi.getData("id_conductor"),mIdTrip,String.valueOf(mLatitudLlegada),String.valueOf(getmLongitudLlegada));
    }

    @OnClick(R.id.mAlertNearClientButton)
    public void OnClickAlertarCliente(){
        mTakeTripPresenter.alertarPasajero(mPrefsTaxsi.getData("latitude"),mPrefsTaxsi.getData("longitude"));
    }

    @OnClick(R.id.mCallRoute)
    public void OnClickRoute(){
        String uri = "";
        Intent intent;
        try {
            if(isStartTrip || takeTripInRoute) {
                uri = String.format(Locale.ENGLISH, "geo:" + Double.valueOf(mPrefsTaxsi.getData("latitude")) + "," + mPrefsTaxsi.getData("longitude") + "?q=" + mLatitudLlegada + "," + getmLongitudLlegada + " (" + "Viaje" + ")");
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        } catch (ActivityNotFoundException ex) {
            try {
                Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(unrestrictedIntent);
            } catch (ActivityNotFoundException innerEx) {
            }
        }
    }

    @Override
    public void OnPossitiveOption() {
        mTakeTripPresenter.cancelTrip(mPrefsTaxsi.getData("id_conductor"),mIdTrip);
    }

    @Override
    public void OnSuccessGetTrip(GetDataTripResponse mGetDataTripResponse) {
        mIdTrip = mGetDataTripResponse.viaje.get(0).id;
        mPrefsTaxsi.saveData("id_viaje",mIdTrip);

        mViajeInCourse = mGetDataTripResponse.viaje.get(0);
        saveDataTrip(mGetDataTripResponse.viaje.get(0));

        nameClient = mGetDataTripResponse.viaje.get(0).nombrePasajero;
        phoneClient = mGetDataTripResponse.viaje.get(0).telPasajero;

        mFromAddressTextView.setText(mGetDataTripResponse.viaje.get(0).lugarPartida);
        mNameClientTextView.setText(mGetDataTripResponse.viaje.get(0).nombrePasajero);
        mToAddressTextView.setText(mGetDataTripResponse.viaje.get(0).lugarLlegada);

        mLatitudPartida = mViajeInCourse.latitudPartida;
        mLongitudPartida = mViajeInCourse.longitudPartida;

        saveIDS();

        showViewEnRutaDeRecoleccion();

        mFragment.drawRoute(new LatLng(Double.parseDouble(mPrefsTaxsi.getData("latitude")), Double.parseDouble(mPrefsTaxsi.getData("longitude"))),new LatLng(Double.parseDouble(mLatitudPartida), Double.parseDouble(mLongitudPartida)),evenlocation);
    }

    @Override
    public void OnSuccessTakeTrip(GetDataTripResponse mGetDataTripResponse) {
        unSubscribe();
        takeTripInRoute = true;
        mTakeTripPresenter.getDataTrip(mPrefsTaxsi.getData("id_conductor"),mGetDataTripResponse.viaje.get(0).id);
        mFragment.drawRoute(new LatLng(Double.parseDouble(mPrefsTaxsi.getData("latitude")), Double.parseDouble(mPrefsTaxsi.getData("longitude"))),new LatLng(Double.parseDouble(mGetDataTripResponse.viaje.get(0).latitudPartida), Double.parseDouble(mGetDataTripResponse.viaje.get(0).longitudPartida)),evenlocation);
        startListenerCancel();
    }

    @Override
    public void OnSuccessCancel() {
        subscribe();
        stoptimertask();
        cleanInternalDataTrip();
        collapseAllView();
        MessageUtils.toast(this, "Viaje Cancelado");
    }

    @Override
    public void OnSuccesInitTrip(GetDataTripResponse mGetDataTripResponse) {
        stoptimertask();
        takeTripInRoute = false;
        isStartTrip = true;
        mFragment.setfirstMoveDraw();
        mPrefsTaxsi.saveData("tripInCourse","1");
        mPrefsTaxsi.removeData("takeTrip");

        mLatitudLlegada = mViajeInCourse.latitudLlegadaSol;
        getmLongitudLlegada = mViajeInCourse.longitudLlegadaSol;

        showViewEnViajeDestino();

        mFragment.drawRoute(new LatLng(Double.parseDouble(mPrefsTaxsi.getData("latitude")), Double.parseDouble(mPrefsTaxsi.getData("longitude"))),new LatLng(Double.parseDouble(mLatitudLlegada), Double.parseDouble(getmLongitudLlegada)),evenlocation);
    }

    @Override
    public void OnEndTrip(GetDataTripResponse mGetDataTripResponse) {
        Intent intent = new Intent(this, EvaluateClientActivity.class);
        intent.putExtra("id_viaje",mIdTrip);
        intent.putExtra("nombre_pasajero",mViajeInCourse.nombrePasajero);

        cleanInternalDataTrip();
        collapseAllView();
        subscribe();
        startActivity(intent);
    }

    @Override
    public void OnSuccesGetStatusTravel(StatusResponse statusResponse) {
        mIdTrip = statusResponse.status.get(0).id_viaje;
        switch (statusResponse.status.get(0).estatus){
                //Conductor Libre
            case "0":
                //deleteDataExist() ;
                break;
                //Conductor en ruta de recoleccion
            case "1":
                getInformationTravelEnRutaDeRecoleccion();
                //showInRouteFromInternalData();
                break;
                //Conductor en viaje
            case "2":
                getInformationTravelEnRutaDeDestino();
                //showStartTripInCourse();
                break;
            case "3":
                //deleteDataExist();
                    break;
            case "4":
               //deleteDataExist();
                    break;
                    default:
                        //deleteDataExist();
        }
    }

    @Override
    public void OnSuccessPendingEvaluation(PendingEvaluationResponse mPendingEvaluationResponse) {
        mTakeTripPresenter.consultingInforationTravel(mPrefsTaxsi.getData("id_conductor"),mPendingEvaluationResponse.mTravel_pending.get(0).id);
    }

    @Override
    public void OnSuccessConsultingInformation(GetDataTripResponse mGetDataTripResponse) {
        Intent intent = new Intent(this, EvaluateClientActivity.class);
        intent.putExtra("id_viaje",mGetDataTripResponse.viaje.get(0).id);
        intent.putExtra("nombre_pasajero",mGetDataTripResponse.viaje.get(0).nombrePasajero );
        startActivity(intent);
    }

    @Override
    public void OnSuccessGetDataTravelEnRutaDeRecoleccion(GetDataTripResponse mGetDataTripResponse) {
        mIdTrip = mGetDataTripResponse.viaje.get(0).id;
        mPrefsTaxsi.saveData("id_viaje",mIdTrip);

        mViajeInCourse = mGetDataTripResponse.viaje.get(0);

        nameClient = mGetDataTripResponse.viaje.get(0).nombrePasajero;
        phoneClient = mGetDataTripResponse.viaje.get(0).telPasajero;

        mLatitudPartida = mViajeInCourse.latitudPartida;
        mLongitudPartida = mViajeInCourse.longitudPartida;

        mFromAddressTextView.setText(mGetDataTripResponse.viaje.get(0).lugarPartida);
        mNameClientTextView.setText(mGetDataTripResponse.viaje.get(0).nombrePasajero);
        mToAddressTextView.setText(mGetDataTripResponse.viaje.get(0).lugarLlegada);

        mContentTraveDetailAndStartrip.setVisibility(View.VISIBLE);
        showViewEnRutaDeRecoleccion();

        getDestinationInfo(mLatitudPartida,
                mLongitudPartida,
                mViajeInCourse.latitudLlegadaSol,
                mViajeInCourse.longitudLlegadaSol);

        mFragment.drawRoute(new LatLng(Double.parseDouble(mPrefsTaxsi.getData("latitude")), Double.parseDouble(mPrefsTaxsi.getData("longitude"))), new LatLng(Double.parseDouble(mLatitudPartida), Double.parseDouble(mLongitudPartida)),evenlocation);
    }

    @Override
    public void OnSuccessGetDataTravelEnViajeDestino(GetDataTripResponse mGetDataTripResponse) {
        stoptimertask();
        isStartTrip = true;
        mViajeInCourse = mGetDataTripResponse.viaje.get(0);
        mPrefsTaxsi.saveData("tripInCourse","1");
        mPrefsTaxsi.removeData("takeTrip");

        nameClient = mGetDataTripResponse.viaje.get(0).nombrePasajero;
        phoneClient = mGetDataTripResponse.viaje.get(0).telPasajero;

        getDestinationInfo(mViajeInCourse.latitudPartida,
                mViajeInCourse.longitudPartida,
                mViajeInCourse.latitudLlegadaSol,
                mViajeInCourse.longitudLlegadaSol);

        mLatitudLlegada = mGetDataTripResponse.viaje.get(0).latitudLlegadaSol;
        getmLongitudLlegada = mGetDataTripResponse.viaje.get(0).longitudLlegadaSol;
        showViewEnViajeDestino();
        mFragment.drawRoute(new LatLng(Double.parseDouble(mPrefsTaxsi.getData("latitude")), Double.parseDouble(mPrefsTaxsi.getData("longitude"))),new LatLng(Double.parseDouble(mLatitudLlegada), Double.parseDouble(getmLongitudLlegada)),evenlocation);
    }


    @Override
    public void OnSuccessCancelListener(GetDataTripResponse mGetDataTripResponse) {
        userCancelTravel(mGetDataTripResponse.viaje.get(0).estatus);
    }

    @Override
    public void OnSuccessGetDataFromNotification(GetDataTripResponse mGetDataTripResponse) {
        if(mGetDataTripResponse.viaje.get(0).idPasajero.equals("0")){
            mCostoTarifaTextView.setText("El costo aproximado de la tarifa es: $0.0 pesos");
        }else{
            getDestinationInfo(mGetDataTripResponse.viaje.get(0).latitudPartida,mGetDataTripResponse.viaje.get(0).longitudPartida,mGetDataTripResponse.viaje.get(0).latitudLlegadaSol, mGetDataTripResponse.viaje.get(0).longitudLlegadaSol);
        }
        mPlaceOriginTextView.setText("De: "+mGetDataTripResponse.viaje.get(0).lugarPartida);
        mClientNameTextView.setText("Cliente: "+mGetDataTripResponse.viaje.get(0).nombrePasajero);
        ExpandAndCollapse.expand(mContentNotificationLinerLayout);
    }

    @Override
    public void OnSuccesCalcularTarifa(CalcularTarifaResponse mCalcularTarifaResponse) {
        saveTarifa(mCalcularTarifaResponse.tarifa.get(0).tarifa);
        if(getDataTrip().idPasajero.equals("0")) {
            mCostoTarifaTextView.setText("El costo aproximado de la tarifa es: $0.0 pesos");
        }else{
            mCostoTarifaTextView.setText("El costo aproximado de la tarifa es: $" + mCalcularTarifaResponse.tarifa.get(0).tarifa + " pesos");
        }
    }

    public void deleteDataExist(){
        if(mViajeInCourse != null){
            cleanInternalDataTrip();
        }
    }

    public void checkOnService(){
        if(!onService){
            enSerivicio();
        }else{
            fueradeservicios();
        }
    }

    public void enSerivicio(){
        onService=true;
        mRichIconImageView.setImageResource(R.drawable.ic_power_on);
        MessageUtils.toast(this, "En servicio");
        subscribe();
    }

    public void fueradeservicios(){
        onService = false;
        mRichIconImageView.setImageResource(R.drawable.ic_power_off);
        MessageUtils.toast(this,"Fuera de servicio");
        unSubscribe();
    }

    public void IsTripInCourse(){
        /*if(mPrefsTaxsi.getData("takeTrip").equals("1")){
            takeTripInRoute = true;
            isSearchCancelTravel = true;
            mViajeInCourse = getDataTrip();
            mPrefsTaxsi.saveData("id_viaje",mViajeInCourse.id);
            mIdTrip = mViajeInCourse.id;
            showInRouteFromInternalData();
        }else{
            if(mPrefsTaxsi.getData("tripInCourse").equals("1")){
                isStartTrip = true;
                mViajeInCourse = getDataTrip();
                mPrefsTaxsi.saveData("id_viaje",mViajeInCourse.id);
                mIdTrip = mViajeInCourse.id;
                showStartTripInCourse();
            }else{
                getStatusPendingTravel();
            }
        }*/
        getStatusPendingTravel();
    }

    public void getIDTripFronInternalStorage(){
        if(!mPrefsTaxsi.getData("id_viaje").equals("")) {
            mIdTrip = mPrefsTaxsi.getData("id_viaje");
        }
    }

    public void saveDataTrip(Viaje trip){
        Gson gson = new Gson();
        String json = gson.toJson(trip);
        mPrefsTaxsi.saveData("viaje",json);
    }

    public Viaje getDataTrip(){
        Gson gson = new Gson();
        String json = mPrefsTaxsi.getData("viaje");
        Viaje obj = gson.fromJson(json, Viaje.class);
        return obj;
    }

    public void setTitle(String mTitle){
        mTitleToolBarTextField.setText(mTitle);
    }

    public void retrieveinfoDriver(){
        mListDriver = (List<Driver>) getIntent().getSerializableExtra("driverInfo");
        setTitle("¡ Hola "+mListDriver.get(0).mNombre+" !");
    }

    public void cleanInternalDataTrip(){
        stoptimertask();
        mFragment.setfirstMoveDraw();
        mPrefsTaxsi.removeData("tripInCourse");
        mPrefsTaxsi.removeData("takeTrip");
        mPrefsTaxsi.removeData("id_viaje");
        mPrefsTaxsi.removeData("viaje");
        mFragment.cleanMap();
        takeTripInRoute = false;
        isStartTrip = false;
    }

    public void showStartTripInCourse(){
        showViewEnViajeDestino();
        mLatitudLlegada = mViajeInCourse.latitudLlegadaSol;
        getmLongitudLlegada = mViajeInCourse.longitudLlegadaSol;
        mFragment.drawRoute(new LatLng(Double.parseDouble(mPrefsTaxsi.getData("latitude")), Double.parseDouble(mPrefsTaxsi.getData("longitude"))),new LatLng(Double.parseDouble(mLatitudLlegada), Double.parseDouble(getmLongitudLlegada)),evenlocation);

    }

    public void showInRouteFromInternalData(){
            nameClient = mViajeInCourse.nombrePasajero;
            phoneClient = mViajeInCourse.telPasajero;
            mFromAddressTextView.setText(mViajeInCourse.lugarPartida);
            mNameClientTextView.setText(mViajeInCourse.nombrePasajero);
            mToAddressTextView.setText(mViajeInCourse.lugarLlegada);

            mLatitudPartida = mViajeInCourse.latitudPartida;
            mLongitudPartida = mViajeInCourse.longitudPartida;
        try {
            mFragment.drawRoute(new LatLng(Double.parseDouble(mPrefsTaxsi.getData("latitude")), Double.parseDouble(mPrefsTaxsi.getData("longitude"))), new LatLng(Double.parseDouble(mLatitudPartida), Double.parseDouble(mLongitudPartida)),evenlocation);
        }catch (NumberFormatException | NullPointerException e){
            e.printStackTrace();
        }
            showViewEnRutaDeRecoleccion();
    }

    public void verifyNotificationTrip(){
        try {
            if (mPrefsTaxsi.getData("Notification").equals("1")) {
                Log.d("Notification", mPrefsTaxsi.getData("id_trip"));
                mIdTrip = mPrefsTaxsi.getData("id_trip");
                mPrefsTaxsi.removeData("Notification");
                ExpandAndCollapse.expand(mContentNotificationLinerLayout);
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    public void isArriveTrip(String mLat, String mLon){
        showStartTripViews();
        /*float meters = 0.0f;
        float requiredDistance = 200.0f;

        double latitude = Double.parseDouble(mLat);
        double longitud = Double.parseDouble(mLon);

        meters = LocationUtils.distance(latitude, longitud,
                Double.valueOf(mLatitudPartida),Double.valueOf(mLongitudPartida));
        if (meters < requiredDistance) {
            showStartTripViews();
        }else{
            dismisStartTripView();
        }*/
    }

    public void showStartTripViews(){
        /*if(expandOnlyTime) {
            expandOnlyTime = false;*/
            mStartTripButton.setVisibility(View.VISIBLE);//ExpandAndCollapse.expandToUp(mStartTripButton);
        //}
    }

    public void dismisStartTripView(){
        mStartTripButton.setVisibility(View.GONE);
    }

    public void subscribe(){
        Log.d("Subscripcion Firebase", "Habilitado");
        FirebaseMessaging.getInstance().subscribeToTopic("c"+mPrefsTaxsi.getData("id_conductor"));
    }

    public void unSubscribe(){
        Log.e("Subscripcion Firebase","Deshabilitado temporalmente");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("c"+mPrefsTaxsi.getData("id_conductor"));
    }


    public void getStatusPendingTravel(){
        mTakeTripPresenter.getStatusTravel(mPrefsTaxsi.getData("id_conductor"));
    }

    public void userCancelTravel(String estatus){
        if(estatus.equals("4")){
            cleanInternalDataTrip();
            stoptimertask();
            mFragment.setfirstMoveDraw();
            Location drawPoint = new Location("");
            drawPoint.setLatitude(Double.parseDouble(mPrefsTaxsi.getData("latitude")));
            drawPoint.setLongitude(Double.parseDouble(mPrefsTaxsi.getData("longitude")));
            mFragment.createMarker(drawPoint);

            MessageUtils.toast(this, "Viaje Cancelado");
            collapseAllView();
            subscribe();
        }
    }

    public void saveIDS(){
        if(!takeTripInRouteFromHistory){
            mPrefsTaxsi.saveData("takeTrip","1");
            takeTripInRoute = true;
        }
    }

    public void startListenerCancel() {
        timer = new Timer();

        initializeTimerTask();

        timer.schedule(timerTask, 0, 45000);
    }

    public void stoptimertask() {
        try {
            timer.cancel();
            timer.purge();
            timer = null;
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Log.e("Verify_Status_Cancel","Listening.....");
                mTakeTripPresenter.getDataTravelCancelListener(mPrefsTaxsi.getData("id_conductor"),mIdTrip);
            }
        };
    }

    public void calificacionPendiente(){
        mTakeTripPresenter.calificacionPendiente(mPrefsTaxsi.getData("id_conductor"));
    }

    public void getInformationTravelEnRutaDeDestino(){
        unSubscribe();
        Log.e("Viaje en Curso","Llevando a pasajero a su destino");
        mTakeTripPresenter.getDataTravelInCourseDestination(mPrefsTaxsi.getData("id_conductor"),mIdTrip);
        mPrefsTaxsi.saveData("tripInCourse","1");
        mPrefsTaxsi.removeData("takeTrip");
        isStartTrip = true;
    }

    public void getInformationTravelEnRutaDeRecoleccion(){
        Log.e("Recogiendo al pasajero","Encuentro con el pasajero");
        unSubscribe();
        mPrefsTaxsi.saveData("takeTrip","1");
        takeTripInRoute = true;
        mTakeTripPresenter.getDataTravelEnRutaDeRecoleccion(mPrefsTaxsi.getData("id_conductor"),mIdTrip);
        startListenerCancel();
    }

    public void showViewEnRutaDeRecoleccion(){
        mInRouteTitleTextView.setText("EN Ruta de recolección");

        mContentNotificationLinerLayout.setVisibility(View.GONE);//ExpandAndCollapse.collapse(mContentNotificationLinerLayout);
        mContentInRoute.setVisibility(View.VISIBLE);//ExpandAndCollapse.expand(mContentInRoute);
        mContentTravelInfo.setVisibility(View.VISIBLE);//ExpandAndCollapse.expandToUp(mContentTravelInfo);

        mContentTraveDetailAndStartrip.setVisibility(View.VISIBLE);//ExpandAndCollapse.expandToUp(mContentTraveDetailAndStartrip);

        mContentEndTrip.setVisibility(View.GONE);//ExpandAndCollapse.expandToBottom(mContentEndTrip);

        mmContentButtonEndTravel.setVisibility(View.GONE);

        mContentTraveDetailAndStartrip.setVisibility(View.VISIBLE);

    }

    public void showViewEnViajeDestino(){
        mInRouteTitleTextView.setText("VIAJE EN TRANSCURSO");
        mContentTraveDetailAndStartrip.setVisibility(View.GONE);//ExpandAndCollapse.expandToBottom(mContentTraveDetailAndStartrip);
        mContentEndTrip.setVisibility(View.VISIBLE);//ExpandAndCollapse.expandToUp(mContentEndTrip);
        mContentInRoute.setVisibility(View.VISIBLE);//ExpandAndCollapse.expand(mContentInRoute);
        mmContentButtonEndTravel.setVisibility(View.VISIBLE);//ExpandAndCollapse.expandToUp(mmContentButtonEndTravel);
        mmContentButtonEndTravel.setVisibility(View.VISIBLE);
    }

    public void collapseAllView(){
        Location drawPoint = new Location("");
        drawPoint.setLatitude(Double.parseDouble(mPrefsTaxsi.getData("latitude")));
        drawPoint.setLongitude(Double.parseDouble(mPrefsTaxsi.getData("longitude")));
        mFragment.createMarker(drawPoint);
        mContentInRoute.setVisibility(View.GONE);//ExpandAndCollapse.collapse(mContentInRoute);
        mContentTravelInfo.setVisibility(View.GONE);//ExpandAndCollapse.expandToBottom(mContentTravelInfo);
        mContentEndTrip.setVisibility(View.GONE);//ExpandAndCollapse.expandToBottom(mContentEndTrip);
        mContentTravelInfo.setVisibility(View.GONE);
        mContentTraveDetailAndStartrip.setVisibility(View.GONE);
    }

    private void getDestinationInfo(String latitudPartida, String longitudPartida, String latitudLlegada, String longitudLlegada) {
        String serverKey = "AIzaSyBuVOjhAn9F4f0wCaorSgczD7LdV0jGbiA"; // Api Key For Google Direction API \\
        final LatLng origin = new LatLng(Double.parseDouble(latitudPartida), Double.parseDouble(longitudPartida));
        final LatLng destination = new LatLng(Double.parseDouble(latitudLlegada), Double.parseDouble(longitudLlegada));

        GoogleDirection.withServerKey(serverKey)
                .from(origin)
                .to(destination)
                .transportMode(TransportMode.DRIVING)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        String status = direction.getStatus();
                        if (status.equals(RequestResult.OK)) {
                            Route route = direction.getRouteList().get(0);
                            Leg leg = route.getLegList().get(0);
                            Info distanceInfo = leg.getDistance();
                            Info durationInfo = leg.getDuration();
                            String distance = distanceInfo.getText();
                            String duration = durationInfo.getText();

                            mTakeTripPresenter.calcularTarifa(distance,duration);

                        } else if (status.equals(RequestResult.NOT_FOUND)) {
                            Log.e("Error","No existe ruta");
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        // Do something here
                    }
                });
    }

    public void saveTarifa(String valueTarife){
        mPrefsTaxsi.saveData(KEY_PRICE_TARIFA, valueTarife);
    }

    public String getTarifa(){
        return mPrefsTaxsi.getData(KEY_PRICE_TARIFA);
    }
}
