package mx.com.sinergiadigital.taxsichoferandroid.Background.Request;

import com.google.gson.annotations.SerializedName;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseRequestInterface;
import java.io.Serializable;

public class EvaluatePassengerRequest implements Serializable,WSBaseRequestInterface {

    @SerializedName("id_conductor")
    public String id_conductor;

    @SerializedName("id_viaje")
    public String id_viaje;

    @SerializedName("valoracion")
    public String valoracion;

    @SerializedName("comentario")
    public String comentario;

    public EvaluatePassengerRequest(String id_conductor, String id_viaje, String valoracion, String comentario) {
        this.id_conductor = id_conductor;
        this.id_viaje = id_viaje;
        this.valoracion = valoracion;
        this.comentario = comentario;
    }
}
