package mx.com.sinergiadigital.taxsichoferandroid.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;

public class Viaje implements Serializable, WSBaseResponseInterface {

    @SerializedName("id")
    public String id;
    @SerializedName("id_pasajero")
    public String idPasajero = "";
    @SerializedName("id_conductor")
    public String idConductor = "";
    @SerializedName("nombre_pasajero")
    public String nombrePasajero = "";
    @SerializedName("nombre_conductor")
    public String nombreConductor = "";
    @SerializedName("tel_pasajero")
    public String telPasajero = "";
    @SerializedName("correo_pasajero")
    public Object correoPasajero = "";
    @SerializedName("tel_conductor")
    public String telConductor = "";
    @SerializedName("correo_conductor")
    public Object correoConductor = "";
    @SerializedName("latitud_conductor")
    public String latitudConductor = "";
    @SerializedName("longitud_conductor")
    public String longitudConductor = "";
    @SerializedName("foto_conductor")
    public String fotoConductor = "";
    @SerializedName("unidad")
    public String unidad = "";
    @SerializedName("modelo")
    public String modelo = "";
    @SerializedName("placas")
    public String placas = "";
    @SerializedName("fecha_solicitud")
    public String fechaSolicitud = "";
    @SerializedName("fecha_inicio")
    public String fechaInicio = "";
    @SerializedName("fecha_llegada")
    public String fechaLlegada = "";
    @SerializedName("lugar_partida")
    public String lugarPartida = "";
    @SerializedName("lugar_llegada")
    public String lugarLlegada = "";
    @SerializedName("latitud_partida")
    public String latitudPartida = "";
    @SerializedName("longitud_partida")
    public String longitudPartida = "";
    @SerializedName("latitud_llegada_sol")
    public String latitudLlegadaSol = "";
    @SerializedName("longitud_llegada_sol")
    public String longitudLlegadaSol = "";
    @SerializedName("latitud_llegada_real")
    public String latitudLlegadaReal = "";
    @SerializedName("longitud_llegada_real")
    public String longitudLlegadaReal = "";
    @SerializedName("estatus")
    public String estatus = "";
    @SerializedName("calificacion_conductor")
    public String calificacion_conductor = "";
    @SerializedName("comentario_conductor")
    public String comentario_conductor = "";
    @SerializedName("calificacion_pasajero")
    public String calificacion_pasajero = "";
    @SerializedName("comentario_pasajero")
    public String comentario_pasajero = "";

}
