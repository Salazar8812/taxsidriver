package mx.com.sinergiadigital.taxsichoferandroid.Background.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;

public class AlertTravelResponse implements WSBaseResponseInterface {
    @SerializedName("resultado")
    public List<PendingEvaluationResponse.Resultado> listResult;
}
