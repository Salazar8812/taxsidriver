package mx.com.sinergiadigital.taxsichoferandroid.Background.Request;

import com.google.gson.annotations.SerializedName;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseRequestInterface;

import java.io.Serializable;

public class LoginRequest implements Serializable,WSBaseRequestInterface{
    @SerializedName("email")
    public String email;

    @SerializedName("password")
    public String password;

    public LoginRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
