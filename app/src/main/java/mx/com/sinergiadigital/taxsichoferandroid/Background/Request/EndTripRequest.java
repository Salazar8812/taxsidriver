package mx.com.sinergiadigital.taxsichoferandroid.Background.Request;

import com.google.gson.annotations.SerializedName;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseRequestInterface;

import java.io.Serializable;

public class EndTripRequest implements Serializable,WSBaseRequestInterface {

    @SerializedName("id_conductor")
    public String id_conductor;

    @SerializedName("id_viaje")
    public String id_viaje;


    @SerializedName("latitud_llegada_real")
    public String latitud_llegada_real;

    @SerializedName("longitud_llegada_real")
    public String longitud_llegada_real;

    public EndTripRequest(String id_conductor, String id_viaje, String latitud_llegada_real, String longitud_llegada_real) {
        this.id_conductor = id_conductor;
        this.id_viaje = id_viaje;
        this.latitud_llegada_real = latitud_llegada_real;
        this.longitud_llegada_real = longitud_llegada_real;
    }
}
