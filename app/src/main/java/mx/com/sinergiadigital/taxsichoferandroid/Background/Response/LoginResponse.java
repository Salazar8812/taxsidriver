package mx.com.sinergiadigital.taxsichoferandroid.Background.Response;

import com.google.gson.annotations.SerializedName;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;
import mx.com.sinergiadigital.taxsichoferandroid.Model.Driver;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LoginResponse implements Serializable, WSBaseResponseInterface {
    @SerializedName("conductor")
    public List<Driver> mDriver = new ArrayList();
}
