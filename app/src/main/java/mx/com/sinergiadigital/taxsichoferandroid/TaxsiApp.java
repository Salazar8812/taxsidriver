package mx.com.sinergiadigital.taxsichoferandroid;

import android.app.Application;
import android.support.multidex.MultiDex;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.BusManager;

/**
 * Created by charlssalazar on 23/05/18.
 */

public class TaxsiApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        BusManager.init();
        MultiDex.install(this);
    }
}
