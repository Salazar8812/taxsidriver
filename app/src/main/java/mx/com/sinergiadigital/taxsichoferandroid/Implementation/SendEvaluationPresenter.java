package mx.com.sinergiadigital.taxsichoferandroid.Implementation;

import android.support.v7.app.AppCompatActivity;

import mx.com.sinergiadigital.taxsichoferandroid.Background.BaseWSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Background.WSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSCallback;

public class SendEvaluationPresenter extends TaxiPresenter implements WSCallback{
    private SendEvaluationCallBack mSendEvaluationCallBack;

    public interface SendEvaluationCallBack{
        void OnSuccessSendEvaluation();
    }

    public SendEvaluationPresenter(AppCompatActivity appCompatActivity, SendEvaluationPresenter.SendEvaluationCallBack mSendEvaluationCallBack) {
        super(appCompatActivity);
        mSendEvaluationCallBack = mSendEvaluationCallBack;
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void sendEvaluation(String id_conductor, String id_cliente, String id_viaje, String comment, String qualify){

    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        super.onSuccessLoadResponse(requestUrl, baseResponse);
    }

}
