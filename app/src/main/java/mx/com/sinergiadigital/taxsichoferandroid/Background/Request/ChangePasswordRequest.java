package mx.com.sinergiadigital.taxsichoferandroid.Background.Request;

import java.io.Serializable;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseRequestInterface;

public class ChangePasswordRequest implements Serializable,WSBaseRequestInterface {
    public String viejoPass;
    public String nuevoPass;
    public String id_conductor;

    public ChangePasswordRequest(String viejoPass, String nuevoPass, String id_conductor) {
        this.viejoPass = viejoPass;
        this.nuevoPass = nuevoPass;
        this.id_conductor = id_conductor;
    }
}
