package mx.com.sinergiadigital.taxsichoferandroid.Background.Response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;
import mx.com.sinergiadigital.taxsichoferandroid.Model.Resultado;
import mx.com.sinergiadigital.taxsichoferandroid.Model.Viaje;

public class GetDataTripResponse implements Serializable, WSBaseResponseInterface {
    @SerializedName("viaje")
    public List<Viaje> viaje = new ArrayList<>();
    @SerializedName("resultado")
    public List<Resultado> resultado = new ArrayList<>();
}
