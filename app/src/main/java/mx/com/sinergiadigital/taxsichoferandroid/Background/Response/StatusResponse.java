package mx.com.sinergiadigital.taxsichoferandroid.Background.Response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;
import mx.com.sinergiadigital.taxsichoferandroid.Model.Status;

public class StatusResponse implements Serializable, WSBaseResponseInterface {
    @SerializedName("estatus")
    public List<Status> status= new ArrayList<>();
}
