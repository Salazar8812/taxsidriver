package mx.com.sinergiadigital.taxsichoferandroid.Implementation;

import android.support.v7.app.AppCompatActivity;

import mx.com.sinergiadigital.taxsichoferandroid.Background.BaseWSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.CloseSessionRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.CloseSessionResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.WSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSCallback;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.MessageUtils;


public class CloseSessionPresenter extends TaxiPresenter implements WSCallback {
    public interface CloseSessionCallback {
        void OnCloseSession();
    }

    public CloseSessionCallback mCloseSessionCallback;

    public CloseSessionPresenter(AppCompatActivity appCompatActivity, CloseSessionCallback mCloseSessionCallback) {
        super(appCompatActivity);
        this.mCloseSessionCallback = mCloseSessionCallback;
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void CloseSession(String id_Conductor){
        CloseSessionRequest closeSessionRequest = new CloseSessionRequest(id_Conductor);
        mWSManager.requestWs(CloseSessionResponse.class, WSManager.WS.CLOSE_SESSION, closeSessionRequest);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
        if(requestUrl.equals(WSManager.WS.CLOSE_SESSION)){
            OnSuccessCloseSession((CloseSessionResponse) baseResponse);
        }
    }

    public void OnSuccessCloseSession(CloseSessionResponse closeSessionResponse){
        if(closeSessionResponse != null){
            if(closeSessionResponse.mListResultado.get(0).resultado == 1) {
                mCloseSessionCallback.OnCloseSession();
            }
        }
    }
}
