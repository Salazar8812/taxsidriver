package mx.com.sinergiadigital.taxsichoferandroid.Background.Response;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;

public class PhonesResponse implements WSBaseResponseInterface {
    @SerializedName("telefono")
    public List<Phone> mListPhones = new ArrayList<>();

    public class Phone{
        @SerializedName("telefono")
        public String mPhone;
    }
}


