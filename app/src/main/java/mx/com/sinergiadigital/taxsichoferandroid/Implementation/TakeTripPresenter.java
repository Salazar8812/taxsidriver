package mx.com.sinergiadigital.taxsichoferandroid.Implementation;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import mx.com.sinergiadigital.taxsichoferandroid.Background.BaseWSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.AlertTravelRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.CalcularTarifaRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.EndTripRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.LocationDriverRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.PendingEvaluateRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.StatusDriverRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.TakeTripRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.AlertTravelResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.CalcularTarifaResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.GetDataTripResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.LoginResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.PendingEvaluationResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.StatusResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.WSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSCallback;
import mx.com.sinergiadigital.taxsichoferandroid.Model.Resultado;
import mx.com.sinergiadigital.taxsichoferandroid.Model.Status;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.MessageUtils;

public class TakeTripPresenter extends TaxiPresenter implements WSCallback {

    private TakeTripCallBack mTakeTripCallBack;

    public interface TakeTripCallBack {
        void OnSuccessGetTrip(GetDataTripResponse mGetDataTripResponse);
        void OnSuccessTakeTrip(GetDataTripResponse mGetDataTripResponse);
        void OnSuccessCancel();
        void OnSuccesInitTrip(GetDataTripResponse mGetDataTripResponse);
        void OnEndTrip(GetDataTripResponse mGetDataTripResponse);
        void OnSuccesGetStatusTravel(StatusResponse statusResponse);
        void OnSuccessPendingEvaluation(PendingEvaluationResponse mPendingEvaluationResponse);
        void OnSuccessConsultingInformation(GetDataTripResponse mGetDataTripResponse);
        void OnSuccessGetDataTravelEnRutaDeRecoleccion(GetDataTripResponse mGetDataTripResponse);
        void OnSuccessGetDataTravelEnViajeDestino(GetDataTripResponse mGetDataTripResponse);
        void OnSuccessCancelListener(GetDataTripResponse mGetDataTripResponse);
        void OnSuccessGetDataFromNotification(GetDataTripResponse mGetDataTripResponse);
        void OnSuccesCalcularTarifa(CalcularTarifaResponse mCalcularTarifaResponse);
    }

    public TakeTripPresenter(AppCompatActivity appCompatActivity, TakeTripCallBack takeTripCallBack) {
        super(appCompatActivity);
        mTakeTripCallBack = takeTripCallBack;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public void onRequestWS(String requestUrl) {

    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void getDataFromNotification(String id_conductor, String id_trip){
        TakeTripRequest takeTripRequest = new TakeTripRequest(id_conductor,id_trip);
        mWSManager.requestWs(GetDataTripResponse.class, WSManager.WS.GET_DATA_FROM_NOTIFICATION, takeTripRequest);
    }

    public void takeTrip(String id_conductor, String id_trip){
        MessageUtils.progress(mAppCompatActivity, "Cargando...");
        TakeTripRequest takeTripRequest = new TakeTripRequest(id_conductor,id_trip);
        mWSManager.requestWs(GetDataTripResponse.class, WSManager.WS.TAKETRIP, takeTripRequest);
    }

    public void getDataTrip(String id_conductor,String id_trip){
        TakeTripRequest takeTripRequest = new TakeTripRequest(id_conductor,id_trip);
        mWSManager.requestWs(GetDataTripResponse.class, WSManager.WS.GET_DATA_TRIP, takeTripRequest);
    }


    public void initTrip(String id_conductor, String id_trip){
        MessageUtils.progress(mAppCompatActivity, "Cargando...");
        TakeTripRequest takeTripRequest = new TakeTripRequest(id_conductor,id_trip);
        mWSManager.requestWs(GetDataTripResponse.class, WSManager.WS.INIT_TRIP, takeTripRequest);
    }

    public void endTrip(String id_conductor, String id_trip, String latitud_llegada_real, String longitud_llegada_real){
        MessageUtils.progress(mAppCompatActivity, "Cargando...");
        EndTripRequest takeTripRequest = new EndTripRequest(id_conductor,id_trip,latitud_llegada_real,longitud_llegada_real);
        mWSManager.requestWs(GetDataTripResponse.class, WSManager.WS.END_TRIP, takeTripRequest);
    }

    public void cancelTrip(String id_conductor, String id_trip){
        MessageUtils.progress(mAppCompatActivity, "Cargando...");
        TakeTripRequest takeTripRequest = new TakeTripRequest(id_conductor,id_trip);
        mWSManager.requestWs(GetDataTripResponse.class, WSManager.WS.CANCEL_TRIP, takeTripRequest);
    }

    public void sendLocationDriver(String id_conductor, String latitud, String longitud){
        LocationDriverRequest locationDriverRequest = new LocationDriverRequest(id_conductor,latitud,longitud);
        mWSManager.requestWs(GetDataTripResponse.class, WSManager.WS.SEND_LOCATION_DRIVER, locationDriverRequest);
    }

    public void getStatusTravel(String id_conductor){
        StatusDriverRequest statusDriverRequest = new StatusDriverRequest(id_conductor);
        mWSManager.requestWs(StatusResponse.class, WSManager.WS.GET_ESTATUS_DRIVER,statusDriverRequest);
    }

    public void calificacionPendiente(String id_driver){
        PendingEvaluateRequest pendingEvaluateRequest = new PendingEvaluateRequest(id_driver);
        mWSManager.requestWs(PendingEvaluationResponse.class, WSManager.WS.GET_PENDING_TRAVEL,pendingEvaluateRequest);
    }

    public void consultingInforationTravel(String id_conductor, String id_trip){
        TakeTripRequest takeTripRequest = new TakeTripRequest(id_conductor,id_trip);
        mWSManager.requestWs(GetDataTripResponse.class, WSManager.WS.GET_DATA_CONSULTING, takeTripRequest);
    }

    public void getDataTravelEnRutaDeRecoleccion(String id_conductor,String id_trip){
        TakeTripRequest takeTripRequest = new TakeTripRequest(id_conductor,id_trip);
        mWSManager.requestWs(GetDataTripResponse.class, WSManager.WS.GET_DATA_TRAVEL_EN_RUTA_DE_RECOLECCION, takeTripRequest);
    }

    public void getDataTravelInCourseDestination(String id_conductor,String id_trip){
        TakeTripRequest takeTripRequest = new TakeTripRequest(id_conductor,id_trip);
        mWSManager.requestWs(GetDataTripResponse.class, WSManager.WS.GET_DATA_TRABEL_EN_VIAJE_DESTINO, takeTripRequest);
    }

    public void getDataTravelCancelListener(String id_conductor,String id_trip){
        TakeTripRequest takeTripRequest = new TakeTripRequest(id_conductor,id_trip);
        mWSManager.requestWs(GetDataTripResponse.class, WSManager.WS.GET_DATA_CANCEL_LISTENER, takeTripRequest);
    }

    public void calcularTarifa(String distancia, String tiempo){
        CalcularTarifaRequest calcularTarifaRequest = new CalcularTarifaRequest(distancia,tiempo);
        mWSManager.requestWs(CalcularTarifaResponse.class, WSManager.WS.CALCULAR_TARIFA, calcularTarifaRequest);
    }

    public void alertarPasajero(String latitud, String longitud){
        AlertTravelRequest alertTravelRequest = new AlertTravelRequest(latitud,longitud);
        mWSManager.requestWs(AlertTravelResponse.class, WSManager.WS.ALERT_NEAR_TRAVEL, alertTravelRequest);
    }


    public void OnSuccessInitTrip(String requestUrl, GetDataTripResponse getDataTripResponse){
        if(getDataTripResponse.resultado.get(0).resultado == 1){
            MessageUtils.stopProgress();
            mTakeTripCallBack.OnSuccesInitTrip(getDataTripResponse);
        }else{
            MessageUtils.stopProgress();
            MessageUtils.toast(mAppCompatActivity,"Ocurrio un error al tratar de iniciar el viaje, intente nuevamente");
        }
    }


    public void OnSuccessGetDataFromNotification(String requesURL, GetDataTripResponse getDataTripResponse){
        try {
            if (getDataTripResponse.resultado.get(0).resultado == 1) {
                mTakeTripCallBack.OnSuccessGetDataFromNotification(getDataTripResponse);
            } else {
                MessageUtils.toast(mAppCompatActivity,"Ocurrio un error al tratar de obtener datos del viaje");
            }
        }catch (NullPointerException e){
            e.printStackTrace();
            MessageUtils.toast(mAppCompatActivity,"Ocurrio un error al tratar de obtener datos del viaje");
        }

    }

    public void OnSuccessTakeTrip(String requestUrl, GetDataTripResponse getDataTripResponse){
        MessageUtils.stopProgress();
        if(getDataTripResponse.resultado.get(0).resultado == 1){
            mTakeTripCallBack.OnSuccessTakeTrip(getDataTripResponse);
        }else{
            MessageUtils.toast(mAppCompatActivity,"El viaje ya fue aceptado por otro taxi");
        }

    }

    public void OnSuccessGetDataTrip(String requestUrl, GetDataTripResponse getDataTripResponse){
        MessageUtils.stopProgress();
        if(getDataTripResponse.resultado.get(0).resultado == 1){
            mTakeTripCallBack.OnSuccessGetTrip(getDataTripResponse);
        }else{
            MessageUtils.toast(mAppCompatActivity,"No se encontraron datos del viaje");
        }
    }

    public void OnSuccessCancelTrip(String requestUrl, GetDataTripResponse resultado){
        MessageUtils.stopProgress();
        if(resultado.resultado.get(0).resultado == 1){
            mTakeTripCallBack.OnSuccessCancel();
        }else{
            MessageUtils.toast(mContext,"Ocurrio un error al cancelar el viaje, intente nuevamente");
        }
    }

    public void OnSuccessSendLocation(String requestUrl,GetDataTripResponse getDataTripResponse){
        MessageUtils.stopProgress();
        if(getDataTripResponse.resultado.get(0).resultado != null){
            Log.d("Coordenadas Enviadas","Coordenadas Actualizadas");
        }else{
            Log.d("Coordenadas No Enviadas","Ocurrio un error al actualizar coordenadas");
        }
    }

    public void OnEndTrip(String requestURL, GetDataTripResponse getDataTripResponse){
        MessageUtils.stopProgress();
        if(getDataTripResponse.resultado.get(0).resultado == 1){
            mTakeTripCallBack.OnEndTrip(getDataTripResponse);
        }else{
            MessageUtils.toast(mAppCompatActivity,"Ocurrio un error al tratar de finalizar el viaje, intente nuevamente");
        }
    }

    public void OnSucccessGetStatus(String requestURL, StatusResponse statusResponse){
        if(statusResponse.status.get(0).id_viaje != null){
            //MessageUtils.stopProgress();
            mTakeTripCallBack.OnSuccesGetStatusTravel(statusResponse);
        }else{
            //MessageUtils.stopProgress();
        }
    }

    public void OnSuccessGetPendingEvaluation(String  requestURL, PendingEvaluationResponse mPendingEvaluationResponse){
        if(mPendingEvaluationResponse.mResultado.get(0).resultado.equals("1")){
            mTakeTripCallBack.OnSuccessPendingEvaluation(mPendingEvaluationResponse);
        }
    }

    public void OnSuccessConsultingDataTravel(String requestURL,GetDataTripResponse mGetDataTripResponse){
        if(mGetDataTripResponse.resultado.get(0).resultado == 1){
            mTakeTripCallBack.OnSuccessConsultingInformation(mGetDataTripResponse);
        }
    }

    public void OnSuccessGetDataTravelEnRutaDeRecoleccion(String requestUrl, GetDataTripResponse getDataTripResponse){
        MessageUtils.stopProgress();

        if(getDataTripResponse.resultado.get(0).resultado == 1){
            mTakeTripCallBack.OnSuccessGetDataTravelEnRutaDeRecoleccion(getDataTripResponse);
        }else{
            MessageUtils.toast(mAppCompatActivity,"No se encontraron datos del viaje");
        }
    }

    public void OnSuccessGetDataTravelEnViajeDestino(String requestUrl, GetDataTripResponse getDataTripResponse){
        MessageUtils.stopProgress();
        if(getDataTripResponse.resultado.get(0).resultado == 1){
            mTakeTripCallBack.OnSuccessGetDataTravelEnViajeDestino(getDataTripResponse);
        }else{
            MessageUtils.toast(mAppCompatActivity,"No se encontraron datos del viaje");
        }
    }

    public void OnSuccessGetDataTravelCancelListener(String requestUrl, GetDataTripResponse getDataTripResponse){
        MessageUtils.stopProgress();

        if(getDataTripResponse.resultado.get(0).resultado == 1){
            mTakeTripCallBack.OnSuccessCancelListener(getDataTripResponse);
        }else{
            /*MessageUtils.stopProgress();
            MessageUtils.toast(mAppCompatActivity,"No se encontraron datos del viaje");*/
        }
    }

    public void OnSuccesCalcularTarifa(CalcularTarifaResponse calcularTarifaResponse){
        try{
            if(!calcularTarifaResponse.tarifa.get(0).tarifa.equals("")) {
                mTakeTripCallBack.OnSuccesCalcularTarifa(calcularTarifaResponse);
            }else{
                MessageUtils.toast(mAppCompatActivity, "Ocurrio un error al calcular tarifa");
            }
        }catch (NullPointerException e){
            e.printStackTrace();
            MessageUtils.toast(mAppCompatActivity, "Ocurrio un error al calcular tarifa");
        }
    }

    public void OnSuccesAlertTravel(AlertTravelResponse alertTravelResponse){
        if(alertTravelResponse != null){
            if(alertTravelResponse.listResult.get(0).resultado.equals("0")){
                MessageUtils.toast(mAppCompatActivity, "Ocurrio un error al envaiar la Alerta");
                Log.e("Error", "Ocurrio un  error");
            }else{
                Log.e("Exito", "Exito al enviar");
                MessageUtils.toast(mAppCompatActivity, "Alerta enviada correctamente");
            }
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        switch (requestUrl){
            case WSManager.WS.GET_DATA_TRIP:
                OnSuccessGetDataTrip(requestUrl, (GetDataTripResponse) baseResponse);
                break;
            case WSManager.WS.TAKETRIP:
                OnSuccessTakeTrip(requestUrl,(GetDataTripResponse) baseResponse);
                break;
            case WSManager.WS.CANCEL_TRIP:
                OnSuccessCancelTrip(requestUrl, (GetDataTripResponse) baseResponse);
                break;
            case WSManager.WS.SEND_LOCATION_DRIVER:
                OnSuccessSendLocation(requestUrl, (GetDataTripResponse) baseResponse);
                break;
            case WSManager.WS.INIT_TRIP:
                OnSuccessInitTrip(requestUrl, (GetDataTripResponse) baseResponse);
                break;
            case WSManager.WS.END_TRIP:
                OnEndTrip(requestUrl, (GetDataTripResponse) baseResponse);
                break;
            case WSManager.WS.GET_ESTATUS_DRIVER:
                OnSucccessGetStatus(requestUrl, (StatusResponse) baseResponse);
                break;
            case WSManager.WS.GET_PENDING_TRAVEL:
                OnSuccessGetPendingEvaluation(requestUrl, (PendingEvaluationResponse) baseResponse);
                break;
            case WSManager.WS.GET_DATA_CONSULTING:
                OnSuccessConsultingDataTravel(requestUrl, (GetDataTripResponse) baseResponse);
                break;
            case WSManager.WS.GET_DATA_TRAVEL_EN_RUTA_DE_RECOLECCION:
                OnSuccessGetDataTravelEnRutaDeRecoleccion(requestUrl, (GetDataTripResponse) baseResponse);
                break;
            case WSManager.WS.GET_DATA_TRABEL_EN_VIAJE_DESTINO:
                OnSuccessGetDataTravelEnViajeDestino(requestUrl, (GetDataTripResponse) baseResponse);
                break;
            case WSManager.WS.GET_DATA_CANCEL_LISTENER:
                OnSuccessGetDataTravelCancelListener(requestUrl, (GetDataTripResponse) baseResponse);
                break;
            case WSManager.WS.GET_DATA_FROM_NOTIFICATION:
                OnSuccessGetDataFromNotification(requestUrl, (GetDataTripResponse) baseResponse);
                break;
            case WSManager.WS.CALCULAR_TARIFA:
                OnSuccesCalcularTarifa((CalcularTarifaResponse) baseResponse);
                break;
            case WSManager.WS.ALERT_NEAR_TRAVEL:
                OnSuccesAlertTravel((AlertTravelResponse) baseResponse);
                break;
        }
    }

}
