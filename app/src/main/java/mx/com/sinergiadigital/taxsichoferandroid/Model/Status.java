package mx.com.sinergiadigital.taxsichoferandroid.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;

public class Status implements Serializable, WSBaseResponseInterface {
    @SerializedName("estatus")
    public String estatus;
    @SerializedName("id_viaje")
    public String id_viaje;
}
