package mx.com.sinergiadigital.taxsichoferandroid.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mx.com.sinergiadigital.taxsichoferandroid.Model.Viaje;
import mx.com.sinergiadigital.taxsichoferandroid.R;

/**
 * Created by charlssalazar on 29/05/18.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryHolder>{

    private Activity activity;
    private List<Viaje> listHistory = new ArrayList<>();

    public HistoryAdapter(Activity activity, List<Viaje> listHistory){
        this.activity = activity;
        this.listHistory = listHistory;
    }

    @Override
    public HistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history_trip, parent, false);
        HistoryHolder vHolder = new HistoryHolder(layoutView);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(HistoryHolder holder, int position) {
        holder.mNameTextView.setText(listHistory.get(position).nombrePasajero);
        holder.mDateTextView.setText(listHistory.get(position).fechaSolicitud);
        holder.mFromTextView.setText(listHistory.get(position).lugarPartida);
        holder.mToTextView.setText(listHistory.get(position).lugarLlegada);
        try{
            holder.mPointGetTextView.setText(listHistory.get(position).calificacion_pasajero);
            holder.mPointLeaveTextView.setText(listHistory.get(position).calificacion_conductor);
            holder.mCommentTextView.setText(listHistory.get(position).comentario_pasajero);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listHistory.size();
    }

    class HistoryHolder extends RecyclerView.ViewHolder{
        private TextView mNameTextView;
        private TextView mDateTextView;
        private TextView mFromTextView;
        private TextView mToTextView;
        private TextView mPointGetTextView;
        private TextView mPointLeaveTextView;
        private TextView mCommentTextView;

        public HistoryHolder(View itemView) {
            super(itemView);
            mNameTextView = itemView.findViewById(R.id.mNameTextView);
            mDateTextView = itemView.findViewById(R.id.mDateTextView);
            mFromTextView = itemView.findViewById(R.id.mFromTextView);
            mToTextView = itemView.findViewById(R.id.mToTextView);
            mPointGetTextView = itemView.findViewById(R.id.mPointGetTextView);
            mPointLeaveTextView = itemView.findViewById(R.id.mPointLeaveTextView);
            mCommentTextView = itemView.findViewById(R.id.mCommentTextView);
        }
    }

}

