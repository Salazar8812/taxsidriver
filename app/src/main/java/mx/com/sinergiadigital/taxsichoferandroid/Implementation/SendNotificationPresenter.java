package mx.com.sinergiadigital.taxsichoferandroid.Implementation;

import android.support.v7.app.AppCompatActivity;

import mx.com.sinergiadigital.taxsichoferandroid.Background.BaseWSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.MessageNotificationRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.TakeTripRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.MessageNotificationResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.PhonesResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.WSBaseRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.WSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSCallback;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.MessageUtils;

public class SendNotificationPresenter  extends TaxiPresenter implements WSCallback {
    private CallCentralCallback mCallCentralCallback;

    public SendNotificationPresenter(AppCompatActivity appCompatActivity,CallCentralCallback mCallCentralCallback) {
        super(appCompatActivity);
        this.mCallCentralCallback = mCallCentralCallback;
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void sendNotification(String id_conductor, String msj){
        MessageNotificationRequest takeTripRequest = new MessageNotificationRequest(id_conductor,msj);
        mWSManager.requestWs(MessageNotificationResponse.class, WSManager.WS.SEND_NOTIFICATION, takeTripRequest);
    }

    public void getCentralPhone(){
        mWSManager.requestWs(PhonesResponse.class, WSManager.WS.GET_CENTRAL_PHONES, new WSBaseRequest());
    }

    public void OnSuccessSendNotification() {
        MessageUtils.stopProgress();
    }

    public void OnSuccessGetCentralPhones(PhonesResponse phonesResponse){
        if(phonesResponse != null){
            if(!phonesResponse.mListPhones.get(0).mPhone.equals("")){
                mCallCentralCallback.OnCallCentral(phonesResponse.mListPhones.get(0).mPhone);
            }
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        switch (requestUrl){
            case WSManager.WS.SEND_NOTIFICATION:
                OnSuccessSendNotification();
                break;

            case WSManager.WS.GET_CENTRAL_PHONES:
                OnSuccessGetCentralPhones((PhonesResponse) baseResponse);
                break;
            default:break;
            }
    }

    public interface CallCentralCallback{
        void OnCallCentral(String phone);
    }

}
