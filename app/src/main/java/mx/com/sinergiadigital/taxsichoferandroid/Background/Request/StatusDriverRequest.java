package mx.com.sinergiadigital.taxsichoferandroid.Background.Request;

import java.io.Serializable;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseRequestInterface;

public class StatusDriverRequest implements Serializable,WSBaseRequestInterface {
    public String id_driver;

    public StatusDriverRequest(String id_driver) {
        this.id_driver = id_driver;
    }
}
