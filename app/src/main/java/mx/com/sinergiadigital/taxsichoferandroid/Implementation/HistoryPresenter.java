package mx.com.sinergiadigital.taxsichoferandroid.Implementation;

import android.support.v7.app.AppCompatActivity;

import java.util.List;

import mx.com.sinergiadigital.taxsichoferandroid.Background.BaseWSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.TakeTripRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Response.GetDataTripResponse;
import mx.com.sinergiadigital.taxsichoferandroid.Background.WSManager;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;
import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSCallback;
import mx.com.sinergiadigital.taxsichoferandroid.Model.Viaje;
import mx.com.sinergiadigital.taxsichoferandroid.Utils.MessageUtils;

public class HistoryPresenter extends TaxiPresenter implements WSCallback {

    public interface HistoryCallback{
        void OnSuccessHistory(List<Viaje> listViaje);
    }

    private HistoryCallback mHistoryCallback;
    public HistoryPresenter(AppCompatActivity appCompatActivity, HistoryCallback historyCallback)  {
        super(appCompatActivity);
        this.mHistoryCallback = historyCallback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void getHistorial(String id_conductor,String id_viaje){
        MessageUtils.progress(mAppCompatActivity, "Cargando...");
        TakeTripRequest takeTripRequest = new TakeTripRequest(id_conductor,id_viaje);
        mWSManager.requestWs(GetDataTripResponse.class, WSManager.WS.HISTORY_TRAVELS, takeTripRequest);
    }

    public void OnSuccessHistory(GetDataTripResponse getDataTripResponse){
        MessageUtils.stopProgress();
        if(getDataTripResponse.resultado != null) {
            if(getDataTripResponse.resultado.get(0).resultado == 1) {
                mHistoryCallback.OnSuccessHistory(getDataTripResponse.viaje);
            }
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if(requestUrl.equals(WSManager.WS.HISTORY_TRAVELS)){
            OnSuccessHistory((GetDataTripResponse) baseResponse);
        }
    }
}
