package mx.com.sinergiadigital.taxsichoferandroid.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;

public class Resultado implements Serializable, WSBaseResponseInterface {

    @SerializedName("resultado")
    public Integer resultado;
}

