package mx.com.sinergiadigital.taxsichoferandroid.Background.Response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.sinergiadigital.taxsichoferandroid.Callbacks.WSBaseResponseInterface;

public class ChangePasswordResponse implements Serializable, WSBaseResponseInterface {
    @SerializedName("resultado")
    public List<PendingEvaluationResponse.Resultado> resultado = new ArrayList<>();
}
