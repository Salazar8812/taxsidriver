package mx.com.sinergiadigital.taxsichoferandroid.Background.Definitions;

import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.EndTripRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.EvaluatePassengerRequest;
import mx.com.sinergiadigital.taxsichoferandroid.Background.Request.TakeTripRequest;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by charlssalazar on 29/05/18.
 */

public interface TaxsiAppDefinitions {
    @GET("/servicios/autenticaConductor.php")
    Call<ResponseBody> login(@Query("email")String email, @Query("password")String password);

    @GET("/servicios/tomaViaje.php")
    Call<ResponseBody> takeTrip(@Query("id_conductor")String id_conductor, @Query("id_viaje")String id_viaje);

    @GET("/servicios/iniciaViaje.php")
    Call<ResponseBody> initTrip(@Query("id_conductor")String id_conductor, @Query("id_viaje")String id_viaje);

    @GET("/servicios/finalizaViaje.php")
    Call<ResponseBody> endTrip(@Query("id_conductor")String id_conductor, @Query("id_viaje")String id_viaje,@Query("latitud_llegada_real")String latitud_llegada_real, @Query("longitud_llegada_real")String longitud_llegada_real);

    @GET("/servicios/evaluaPasajero.php")
    Call<ResponseBody> evaluatePassenger(@Query("id_conductor")String id_conductor, @Query("id_viaje")String id_viaje,@Query("valoracion")String valoracion, @Query("comentario")String comentario);

    @GET("/servicios/getDatosViajeConductor.php")
    Call<ResponseBody> getDataTrip(@Query("id_conductor")String id_conductor, @Query("id_viaje")String id_viaje);

    @GET("/servicios/cancelaViajeConductor.php")
    Call<ResponseBody> cancelTrip(@Query("id_conductor")String id_conductor, @Query("id_viaje")String id_viaje);

    @GET("/servicios/actualizaCoordenadasChofer.php")
    Call<ResponseBody> sendLocationDriver(@Query("id_conductor")String id_conductor, @Query("latitud")String latitud, @Query("longitud")String longitud);

    @GET("/servicios/getHistoricoViajesConductor.php")
    Call<ResponseBody> getHistory(@Query("id_conductor")String id_conductor, @Query("id_viaje")String id_viaje);

    @GET("/servicios/getEstatusConductor.php")
    Call<ResponseBody> getStatusDriver(@Query("id_conductor")String id_conductor);

    @GET("/servicios/conductorTieneCalifPendiente.php")
    Call<ResponseBody> getCalificacionPendiente(@Query("id_conductor")String id_conductor);

    @GET("/servicios/enviarMensajeAutomatico.php")
    Call<ResponseBody> sendNotification(@Query("id_conductor")String id_conductor, @Query("msj")String msj);

    @GET("/servicios/updatePasswordConductor.php")
    Call<ResponseBody> changePassword(@Query("id_conductor")String id_conductor, @Query("viejopass")String viejoPass, @Query("nuevopass")String nuevopass);

    @GET("/servicios/calcularTarifa.php")
    Call<ResponseBody> calcularTarifa(@Query("distancia")String id_conductor, @Query("tiempo")String viejoPass);

    @GET("/servicios/getTelefonoCentral.php")
    Call<ResponseBody> getTelefonosCentral();

    @GET("/servicios/alertaPasaje.php")
    Call<ResponseBody> getAlertNearTravel(@Query("latitud") String latitud , @Query("longitud") String longitud);

    @GET("/servicios/cierraSesion.php")
    Call<ResponseBody> closeSession(@Query("id_usuario") String id_usuario);

}
